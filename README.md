# NDL Experiments 

## Requirements
- Java >= 1.8
- Python >= 3.5
- SWI-Prolog >= 7.0
- MiniZinc >= 2.4.2

Experiments expect all of them being added to the PATH.

## Setup

1. Install SWI-Prolog package:\
```# swipl -g "pack_install('noodle-prolog/noodle-0.9.tgz', [interactive(false), upgrade(true)]), halt."```
2. Create Python3 virtual environment:\
```# python3 -m venv ndl-python```
3. Activate virtual environment:\
```# source ndl-python/bin/activate```
4. Install python requirements:\
```# pip install -r requirements.txt```
 
## Run

To run experiment, execute script with its name,i.e.\
```# python run_experiment.py tsp```

## Customization

To customize experiment you have to change contents of the ```problems/<name>``` directory corresponding to you experiment. The PonyGE2 (grammatical evolution) params are available at ```problems/<name>/parameters/default.txt```. 

## TODO:

1. There are only TSP and Graph Colouring problem included. Traveling Tournament is next to be added.
2. Script always runs the experiment from end to end, it should be possible to run only a part of the experiment
3. Script always uses ```default.txt``` parameters, it should be to possible to specify the parameters file
4. Each problem has to have a cost function provided (even empty one) because it is used to evaluate neighborhood. It should be possible to get the cost from MiniZinc model directly.
5. The synthesized neighborhood is run in the Oscar and compared to a basic neighborhood. Oscar model and basic neighborgood have to be provided by the user at the moment. Should be fixed when switching from Prolog/MiniZinc to Oscar backend.

## FAQ:

1. Is there any documentation on the PonyGE2 conifguration file?

Sure, but there is a lot of options... Check https://github.com/PonyGE/PonyGE2/wiki for the documentation. The most important settings are probably ```CORES```, that sets how many CPU cores are used to evaluate individuals, ```POPULATION_SIZE``` controlling the amount of the indivuals in the populations and ```GENERATIONS``` setting how many generations will be considered in the process.  

2. I don't have Java 1.8, what can I do?

Just install it locally, https://adoptopenjdk.net/releases.html contains some prebuilt open-source releases. After unpacking the tarball, set ```$JAVA_HOME``` to the extracted folder and modify ```$PATH``` variable: ```export PATH="$JAVA_HOME/bin:$PATH"``` 


