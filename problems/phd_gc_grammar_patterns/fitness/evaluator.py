#!/usr/bin/env python3

from typing import Dict, List
from sultan.api import Sultan 
from pathlib import Path
from statistics import mean, median
from functools import reduce
from operator import mul

import random
import tempfile 
import sys
import json
import math
import glob
from sys import stdout

class Evaluator:
    def __init__(self, query: str, cwd: str, logging: bool = False, clean: bool = True, test_instances_number = 0, test_states_per_instance: int = 2, time_limit: float = 3):
        self.cwd = cwd
        self.logging = logging
        self.clean = clean
        self.query = query  
        self.weights = [1.0, 1.0, 1.0, 0.4, 0.3, 0.3]
        self.eps = 1e-9
        self.precision_digits = 8
        self.worst_score = self.log_score(self.eps)
        self.test_states_per_instance = test_states_per_instance
        self.time_limit = time_limit
        self.score_names = ["sat_score", "sat_constraint_bonus", "changes_score", "amount_score", "unique_ratio", "unique_cost"]

        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            self.instances = s.ls("instances/*.ndl").run().stdout
            if test_instances_number > 0:
                self.instances = self.instances[:test_instances_number]
            s.mkdir('-p temp').run()
        self.instance_names = [Path(p).stem for p in self.instances]
            
        self.tmpdirname = "{}/{}".format("temp", Path(tempfile.mkdtemp(dir="{}/temp".format(self.cwd))).stem)

    def log_score(self, score):
        return round(math.log(max(score, self.eps)), self.precision_digits)

    def normalized_log_score(self, score):
        return - self.log_score(score) / self.worst_score

    def compile(self, query: str) -> str:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            query_filepath = "{}/query".format(self.tmpdirname)
            s.echo('"{}"'.format(query)).pipe().swipl("-f {} ./query-compiler.pl {}".format(self.instances[0], query_filepath)).run()
            return query_filepath
    
    def evaluate_code(self) -> Dict:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            output = s.swipl("./query-analyzer.pl \"{}\"".format(self.query[0:-1])).run()
            if self.logging:
                print(output)
            return json.loads(str(output))

    def evaluate_neighborhood(self, query_filepath: str, instance: str) -> List[Dict]:
        with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
            output = s.swipl("./query-evaluator.pl {} {} {} {}".format(query_filepath, instance, self.time_limit, self.test_states_per_instance)).run()
            if self.logging:
                print(output)
            result = json.loads(str(output))
            if len(result) == 0:
                return [None] * self.test_states_per_instance
            return result

    def calculate_code_style_score(self, instance:Dict) -> float:
        ignored_outputs_score = None 
        missing_inputs_score = None
        self_loops_score = None
        shadowed_score = None
        effective_score = instance["effective"] / instance["all_terms"]

        n_scores = 1
        total_score =  effective_score

        if instance["ignored_outputs_max"] > 0:
            ignored_outputs_score = 1 - instance["ignored_outputs"] / instance["ignored_outputs_max"]
            total_score += ignored_outputs_score
            n_scores += 1
        if instance["missing_inputs_max"] > 0:
            missing_inputs_score = 1 - instance["missing_inputs"] / instance["missing_inputs_max"]
            total_score += missing_inputs_score
            n_scores += 1
        if instance["self_loops_max"] > 0:
            self_loops_score = 1 - instance["self_loops"] / instance["self_loops_max"]
            total_score += self_loops_score
            n_scores += 1
        if instance["shadowed_max"] > 0:
            shadowed_score = 1 - instance["shadowed"] / instance["shadowed_max"]
            total_score += shadowed_score
            n_scores += 1

        result = total_score / n_scores
        if self.logging:
            print("\n\t".join(["code_style_score =",
                   "ignored_outputs_score = {}".format(ignored_outputs_score),
                   "missing_inputs_score = {}".format(missing_inputs_score),
                   "self_loops_score = {}".format(self_loops_score),
                   "shadowed_score = {}".format(shadowed_score),
                   "effective_score = {}".format(effective_score),
                   "/ {}".format(n_scores),
                   "= {}".format(result)]))
        return result

    def calculate_neighborhood_score(self, instance:Dict) -> float:
        if instance == None:
            values = [0] * 6
            self.neighborhood_score_details_list.append("- {}".format(values))
            return values
        
        def evaluate_sat_score(s:Dict, size:int) -> float:
            if s["min_val"] == 1.0:
                return 1.0
            if s["max_val"] == 1.0:
                return s["avg_val"] / size
            return 0
        
        def logistic(value):
            return 0 if value == 0 else 1/(1 + math.exp(40 * (-value + 0.06)))

        sat_square_sum = sum([s["min_val"] * s["min_val"] for s in instance["neighbors"]["satisfied"].values()]) / len(instance["neighbors"]["satisfied"])
        changes = instance["neighbors"]["changes"]["avg_val"]
        size = instance["instance"]["variables"]
        unsat_penalty = (1 - sat_square_sum)/changes if changes > 0 else size
        normalized_unsat_penalty = unsat_penalty/size
        sat_score = 1 - normalized_unsat_penalty
        
        
        total_amount = instance["neighbors"]["amount"]
        amount = total_amount - instance["neighbors"]["duplicates"]  
        satisfiable_amount = math.factorial(size) / (2 * math.factorial(size - 2)) 
        amount_score = 0 if amount <= 1 else (1.0 if amount > satisfiable_amount else 1/(1 + math.exp(0.5 * (-amount + satisfiable_amount/2))))

        changes_stdev = instance["neighbors"]["changes"]["std_dev"]
        changes_score = logistic(changes_stdev)

        unique_ratio = logistic(amount / total_amount)
        unique_costs = instance["neighbors"]["unique_costs"]
        cost_score = logistic((unique_costs - 1) / amount)
        
        sat_constraint_bonus = sum([evaluate_sat_score(s, size) for s in instance["neighbors"]["satisfied"].values()]) / len(instance["neighbors"]["satisfied"])
        values = [sat_score, sat_constraint_bonus, changes_score, amount_score, unique_ratio, cost_score]
        self.neighborhood_score_details_list.append("- {}".format(values))
        return values

    def evaluate(self):
        code_analysis_results = self.evaluate_code()
        self.optimized_query = code_analysis_results["optimized_query"]
        code_stats = code_analysis_results["stats"]
        is_valid = code_analysis_results["valid"] == 1
        
        self.neighborhood_score = -1
        self.code_style_score = -(1-self.calculate_code_style_score(code_stats))
        self.neighborhood_score_details_list = []
        if is_valid:
            self.neighborhood_score_details_list.append(",".join(self.score_names))
            query_filepath = self.compile(self.optimized_query)
            neighborhood_results = sum([self.evaluate_neighborhood(query_filepath, i) for i in self.instance_names ], [])
            if len(neighborhood_results) > 0:
                def invert(s, c = 1):
                    return (s * c) - 1
                
                scores = [self.calculate_neighborhood_score(i) for i in neighborhood_results]
                worst_sat_score = min(s[0] for s in scores)
                worst_sat_bonus = min(s[1] for s in scores)
                mean_sat_score = mean(s[0] for s in scores)
                mean_sat_bonus = mean(s[1] for s in scores)
                complexity_score = mean(s[2]*s[3] for s in scores)
                number_ratio = mean(s[3] for s in scores)
                unique_ratio = max(s[4] for s in scores) 
                unique_score = mean(s[5] for s in scores)

                shape_score = number_ratio * unique_ratio * unique_score
                symmetric_penalty = 1.0 if (shape_score >= self.eps) else 0.5 
                metrics = [worst_sat_score, worst_sat_bonus, mean_sat_score, mean_sat_bonus, complexity_score, shape_score]
                weights = [0.35, 0.35, 0.05, 0.05, 0.1, 0.1]
                components = list(zip(metrics, weights))
                self.neighborhood_score = invert(sum([(m*w) for m,w in components]) * symmetric_penalty)
                score_label = "{} = (".format(self.neighborhood_score) + " + ".join(["{} * {}".format(m, w) for m,w in components]) + ") * {} - 1".format(symmetric_penalty)
                self.neighborhood_score_details_list.append(score_label)
            
        if self.clean:
            with Sultan.load(cwd=self.cwd, logging=self.logging) as s:
                s.rm("-rf {}".format(self.tmpdirname)).run() 
        self.neighborhood_score_details = "\n".join(["is_valid: {}".format(is_valid)] + self.neighborhood_score_details_list)
        if self.logging:
            print(self.neighborhood_score_details)

if __name__ == "__main__":
    evaluator = Evaluator(sys.argv[1], ".", logging=True, clean=False)
    evaluator.evaluate()
    print("code-style: {}\nneighborhod: {}".format(evaluator.code_style_score, evaluator.neighborhood_score))
