CACHE:                  True
CORES:			        64
MULTICORE:              True
LOOKUP_BAD_FITNESS:	True
DEBUG:                  False
GENERATIONS:            50
GRAMMAR_FILE:           grammar.bnf
INITIALISATION:         PI_grow
INVALID_SELECTION:      False
MUTATION:               subtree
CROSSOVER:		        subtree
CROSSOVER_GAUSSIAN:	False
MUTATION_EVENTS:	    2
POPULATION_SIZE:        500
FITNESS_FUNCTION:       experiment.fitness_function
REPLACEMENT:            generational
SELECTION:              tournament
TOURNAMENT_SIZE:        4
VERBOSE:                False
NOVELTY_BY_GEN:		True
NOVELTY:		True
NOVELTY_ALGORITHM:	fitness
NOVELTY_SELECTION_ALG:	adapt
MUTATE_DUPLICATES:	True
MAX_TREE_DEPTH 6
MAX_TREE_NODES 20
