package Experiment

import java.io._
import java.net.Socket

import NDL.NDLNeighborhood
import TSProblem.Instance.TSPInstanceFactory
import oscar.cbls.core.computation.{CBLSIntConst, IntValue}
import oscar.cbls.core.objective.{CascadingObjective, Objective}
import oscar.cbls.lib.constraint.{AllDiff, NE}
import oscar.cbls.lib.invariant.logic.{Int2Int, IntElement, IntInt2Int}
import oscar.cbls.lib.invariant.numeric.Sum
import oscar.cbls.modeling.CBLSModel
import oscar.cbls.core.search.{Best, First}
import resource.managed

object Basic extends CBLSModel with App {
  val instance = TSPInstanceFactory("gr17")
  // data
  val nodesNum = instance.nodesNum
  val nodesRange = 0 until nodesNum

  // variables
  val order = nodesRange.map(n => CBLSIntVar(n, nodesRange, s"order($n)")).toArray
  c.post(AllDiff(order))

  val distances = nodesRange.map(i => new IntInt2Int(order(i), order((i+1) % nodesNum), (o1,o2) => instance.distance(o1,o2)))
  val optObj = Objective(Sum(distances))
  val satObj = Objective(() => c.violation.value, c.model)
  val obj = new CascadingObjective(satObj, optObj, instance.maxDistance * nodesNum)

  close()

  val neighborhood = swapsNeighborhood(order, selectFirstVariableBehavior = Best(() => 10), selectSecondVariableBehavior = Best(() => 10)).metropolis().maxMoves(1000)
  // neighborhood.verbose = 1
  neighborhood.doAllMoves(obj = obj)
  // println(NDLProfile.selectedStatisticInfo(List(neighborhood)))
  // println(neighborhood.collectThisProfileIterations map { _.toString } mkString("[",",",")"))
  println(obj.detailedString(false))
  println(order.map({
    _.valueString
  }).mkString("[", ",", "]"))
}
