package TSProblem.Instance

import scala.util.Random

case object TSP_random extends TSPEuc2dInstance {
  override val name: String = "random"
  override val nodesNum: Int = 6
  override val maxDistance: Int = 150
  override val coords: Array[(Float, Float)] = Seq.fill(6)((Random.nextFloat()*100, Random.nextFloat()*100)).toArray
}
