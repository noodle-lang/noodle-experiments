package Experiment

import java.io.{BufferedReader, BufferedWriter, InputStreamReader, OutputStreamWriter, PrintWriter}
import java.net.Socket

import Experiment.Instance.GraphColoringInstanceFactory
import oscar.cbls.CBLSModel
import oscar.cbls.core.objective.{CascadingObjective, Objective}
import oscar.cbls.lib.constraint.NE
import NDL.{NDLNeighborhood, NDLProfile}
import oscar.cbls.core.search.Best
import oscar.cbls.lib.invariant.numeric.Nvalue
import oscar.cbls.lib.search.combinators.Profile
import oscar.cbls.lib.search.neighborhoods.{AssignNeighborhood, RandomizeNeighborhood}
import resource.managed

object Basic extends CBLSModel with App {
  val instance = GraphColoringInstanceFactory("queen8_12")
  // data
  val graph = instance.graph

  val nodesNum = graph.length
  var colorsNum = nodesNum
  val nodesRange = 0 until nodesNum
  var colorsRange = 0 until colorsNum

  // variables
  val nodes = nodesRange.map(n => CBLSIntVar(n, colorsRange, s"nodes($n)")).toArray
  var maxColor = new Nvalue(nodes)
  for (n1 <- nodesRange; n2 <- nodesRange if graph(n1)(n2) == 1) {
    c.post(NE(nodes(n1), nodes(n2)))
  }

  val optObj = Objective(maxColor)
  val satObj = Objective(() => c.violation.value, c.model)
  val obj = new CascadingObjective(satObj, optObj)

  close()

  var neighborhood = assignNeighborhood(nodes).metropolis().maxMoves(1000)
  neighborhood.doAllMoves(obj = obj)
  println(obj.detailedString(false))
  println(s"violations = ${c.violation}\ncolors = $maxColor")
}
