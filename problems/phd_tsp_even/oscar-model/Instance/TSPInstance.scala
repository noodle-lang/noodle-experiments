package TSProblem.Instance

trait TSPInstance {
  val name: String
  val nodesNum: Int
  val maxDistance: Int
  def distance(start:Int, end:Int): Int
}

trait TSPInstanceExplicit extends TSPInstance {
  val distances:Array[Array[Int]]
  def distance(start:Int, end:Int): Int = distances(start)(end)
}

trait TSPEuc2dInstance extends TSPInstance {
  val coords: Array[(Float,Float)]
  def distance(start:Int, end:Int):Int = {
    val startCoords = coords(start)
    val endCoords = coords(end)
    scala.math.round(scala.math.sqrt(scala.math.pow(startCoords._1 - endCoords._1, 2) + scala.math.pow(startCoords._2 + endCoords._2, 2))).toInt
  }
}

object TSPInstanceFactory {

  def apply(name: String): TSPInstance = {
    name match {
      case "random" => TSP_random
      case "gr17" => TSP_gr17
      case _ => TSPFromFile( s"./ndl/instances/tsp-instances/$name.tsp")
    }
  }
}
