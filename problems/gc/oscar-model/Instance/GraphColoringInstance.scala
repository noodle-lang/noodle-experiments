package Experiment.Instance

trait GraphColoringInstance {
  val name: String
  val graph: Array[Array[Int]]
}

object GraphColoringInstanceFactory {

  def apply(name: String): GraphColoringInstance = {
    if (name == "queen5_5") {
      GC_queen5_5
    } else if (name == "queen8_12") {
      GC_queen8_12
    } else {
      null
    }
  }
}
