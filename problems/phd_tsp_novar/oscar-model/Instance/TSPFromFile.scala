package TSProblem.Instance
import resource.managed
import scala.io.Source

case class TSPFromFile(name:String, nodesNum:Int, maxDistance:Int, coords:Array[(Float,Float)]) extends TSPEuc2dInstance

object TSPFromFile {
  def apply(path:String): TSPEuc2dInstance = {
    var name:String = ""
    var coords:Array[(Float,Float)] = new Array(0)
    var minX:Float = Float.MaxValue
    var maxX:Float = Float.MinValue
    var minY = minX
    var maxY = maxX

    for { f <- managed(Source.fromFile((path)))
          line <- f.getLines()}{
      val values : Array[String] = line.split(":") map { _.trim() }
      if (values.length == 2){
        values(0) match {
          case "NAME" => name = values(1)
          case "EDGE_WEIGHT_TYPE" => if (values(1) != "EUC_2D") { throw new IllegalArgumentException("The TSP file is not of euclidean type") }
          case "DIMENSION" => coords = new Array(values(1).toInt)
          case _ => {}
        }
      } else {
        val node = line.split(' ') map { _.trim() }
        if (node.length == 3) {
          val i = node(0).toInt - 1
          val x = node(1).toFloat
          val y = node(2).toFloat
          coords(i) = (x, y)
          minX = scala.math.min(minX, x)
          minY = scala.math.min(minY, y)
          maxX = scala.math.max(maxX, x)
          maxY = scala.math.max(maxY, y)
        }
      }
    }

    val maxDistance = scala.math.round(scala.math.sqrt(scala.math.pow(maxX - minX, 2) + scala.math.pow(maxY - minY, 2))).toInt
    new TSPFromFile(name, coords.length, maxDistance, coords)
  }
}
