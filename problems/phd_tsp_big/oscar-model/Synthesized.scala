package Experiment

import java.io.{BufferedReader, BufferedWriter, InputStreamReader, OutputStreamWriter, PrintWriter}
import java.net.Socket

import NDL.{NDLNeighborhood, NDLProfile}
import TSProblem.Instance.TSPInstanceFactory
import oscar.cbls.core.computation.{CBLSIntConst, IntValue}
import oscar.cbls.core.objective.{CascadingObjective, Objective}
import oscar.cbls.core.search.{Best, First}
import oscar.cbls.lib.constraint.{AllDiff, NE}
import oscar.cbls.lib.invariant.logic.{Int2Int, IntElement, IntInt2Int}
import oscar.cbls.lib.invariant.numeric.Sum
import oscar.cbls.lib.search.combinators.GuidedLocalSearch
import oscar.cbls.lib.search.neighborhoods.AssignNeighborhood
import oscar.cbls.modeling.CBLSModel
import resource.managed

object Synthesized extends CBLSModel with App {
  val instance = TSPInstanceFactory("gr17")
  // data
  val nodesNum = instance.nodesNum
  val nodesRange = 0 until nodesNum

  // variables
  val next = nodesRange.map(n => CBLSIntVar((n+1) % nodesNum, nodesRange, s"next($n)")).toArray
  val order = new Array[IntValue](nodesNum)
  order(0) = CBLSIntConst(0)
  nodesRange.drop(1).foreach(i => order(i) = IntElement(order(i-1), next))
  c.post(AllDiff(next))
  c.post(AllDiff(order))
  next.zipWithIndex.foreach { case(r,i) => c.post(NE(r, i))}

  val distances = nodesRange.map(i => new Int2Int(next(i), n => instance.distance(i,n)))
  val optObj = Objective(Sum(distances))
  val satObj = Objective(() => c.violation.value, c.model)
  val obj = new CascadingObjective(satObj, optObj, instance.maxDistance * nodesNum)

  close()

  NDLNeighborhood.withConnection
    { (in, out) =>
      val neighborhood = NDLNeighborhood(next, "query", instance.name, in, out, loopBehavior = Best(() => 10)).metropolis().maxMoves(1000)
      // neighborhood.verbose = 1
      neighborhood.doAllMoves(obj = obj)
//      val search = new GuidedLocalSearch(neighborhood, List(obj), true)
//      search.verbose = 1
//      search.doAllMoves(obj = obj);
      // println(NDLProfile.selectedStatisticInfo(List(neighborhood)))
      // println(neighborhood.collectThisProfileIterations map { _.toString } mkString("[",",",")"))
      println(obj.detailedString(false))
      println(next.map({
        _.valueString
      }).mkString("[", ",", "]"))
      println(order.map({
        _.valueString
      }).mkString("[", ",", "]"))
    }
}
