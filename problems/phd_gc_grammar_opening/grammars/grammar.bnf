<program> ::= (neighbor :- <get-template>, <move-get-top>, <filter-template>, <move-filter-top>, <set-template>, <meta-template>, <move-update-top>).

<get-template> ::=
	| value("'nodes'", VAR_NODES_0, RANGE_COLORS_0)
	| variable("'nodes'", RANGE_INDEXES_0,  VAR_NODES_0)
	| constraint("'neq_nodes_nodes_1'", VAR_NODES_0, VAR_NODES_1)

<filter-template> ::= RANGE_COLORS_0 "<" RANGE_COLORS_1
	| RANGE_COLORS_0 "\=" RANGE_COLORS_1
	| RANGE_COLORS_0 = RANGE_COLORS_1
	| RANGE_INDEXES_0 "<" RANGE_INDEXES_0
	| RANGE_INDEXES_0 "\=" RANGE_INDEXES_0
	| RANGE_INDEXES_0 = RANGE_INDEXES_0
	| VAR_NODES_0 "\=" VAR_NODES_1
	| VAR_NODES_0 = VAR_NODES_0
	| is_satisfied("'neq_nodes_nodes_1'", VAR_NODES_0, VAR_NODES_1)
	| is_violated("'neq_nodes_nodes_1'", VAR_NODES_0, VAR_NODES_1)
	| true

<set-template> ::= set_value(VAR_NODES_0, RANGE_COLORS_1) 
	| swap_values(VAR_NODES_0, VAR_NODES_1)
	| flip_variable(VAR_NODES_0, RANGE_COLORS_1, RANGE_COLORS_2)
	| true

<meta-template> ::= for_each(constraint("'neq_nodes_nodes_1'", VAR_NODES_META_0, VAR_NODES_META_1), (<move-get>, <move-filter>, <move-update>))
	| walk_over(constraint("'neq_nodes_nodes_1'", VAR_NODES_META_0, VAR_NODES_META_1), VAR_NODES_0, (<move-get>, <move-filter>, <move-update>))
	| true

<move-get-top> ::= <move-get-top-atomic>, <move-get-top>
    | true

<move-get-top-atomic> ::= constraint("'neq_nodes_nodes_1'", <var-nodes-top>, <var-nodes-top>)
	| range_element("'colors'", <range-colors-top>)
	| range_element("'indexes'", <range-indexes-top>)
	| value("'nodes'", <var-nodes-top>, <range-colors-top>)
	| variable("'nodes'", <range-indexes-top>, <var-nodes-top>)

<move-filter-top> ::= <move-filter-top-atomic>, <move-filter-top>
    | true

<move-filter-top-atomic> ::= <range-colors-top> "<" <range-colors-top>
	| <range-colors-top> "\=" <range-colors-top>
	| <range-colors-top> = <range-colors-top>
	| <range-indexes-top> "<" <range-indexes-top>
	| <range-indexes-top> "\=" <range-indexes-top>
	| <range-indexes-top> = <range-indexes-top>
	| <var-nodes-top> "\=" <var-nodes-top>
	| <var-nodes-top> = <var-nodes-top>
	| is_satisfied("'neq_nodes_nodes_1'", <var-nodes-top>, <var-nodes-top>)
	| is_violated("'neq_nodes_nodes_1'", <var-nodes-top>, <var-nodes-top>)

<move-update-top> ::= <move-update-top-atomic>, <move-update-top>
    | <move-update-top-atomic>

<move-update-top-atomic> ::= flip_variable(<var-nodes-top>, <range-colors-top>, <range-colors-top>)
	| flip_variable(<var-nodes-top>, <range-colors-top>, <range-colors-top>)
	| flip_variable(<var-nodes-top>, <range-colors-top>, <range-colors-top>)
	| flip_variable(<var-nodes-top>, <range-colors-top>, <range-colors-top>)
	| swap_values(<var-nodes-top>, <var-nodes-top>)
    | set_value(<var-nodes-top>, <range-colors-top>)
    | for_each(constraint("'neq_nodes_nodes_1'", VAR_NODES_META_0, VAR_NODES_META_1), (<move-get>, <move-filter>, <move-update>))
	| for_each(range_element("'colors'", RANGE_COLORS_META_0), (<move-get>, <move-filter>, <move-update>))
	| for_each(range_element("'indexes'", RANGE_INDEXES_META_0), (<move-get>, <move-filter>, <move-update>))
	| for_each(variable("'nodes'", RANGE_INDEXES_META_0, VAR_NODES_META_0), (<move-get>, <move-filter>, <move-update>))
	| walk_over(constraint("'neq_nodes_nodes_1'", VAR_NODES_META_0, VAR_NODES_META_1), <var-nodes-top>, (<move-get>, <move-filter>, <move-update>))


<range-colors-meta> ::= RANGE_COLORS_META_<i-values-meta>

<range-colors-top> ::= RANGE_COLORS_<i-values>

<range-colors> ::= <range-colors-meta>
	| <range-colors-top>

<range-indexes-meta> ::= RANGE_INDEXES_META_<i-values-meta>

<range-indexes-top> ::= RANGE_INDEXES_<i-values>

<range-indexes> ::= <range-indexes-meta>
	| <range-indexes-top>

<var-nodes-meta> ::= VAR_NODES_META_<i-variables-meta>

<var-nodes-top> ::= VAR_NODES_<i-variables>

<var-nodes> ::= <var-nodes-meta>
	| <var-nodes-top>

<i-values-meta> ::= GE_RANGE:2

<i-values> ::= GE_RANGE:4

<i-variables-meta> ::= GE_RANGE:2

<i-variables> ::= GE_RANGE:4

<move-get> ::= <move-get-atomic>, <move-get>
    | true

<move-get-atomic> ::= get_value("'nodes'", <var-nodes>, <range-colors>)

<move-filter> ::= <move-filter-atomic>, <move-filter>
    | true

<move-filter-atomic> ::= <range-colors> "<" <range-colors>
	| <range-colors> "\=" <range-colors>
	| <range-colors> = <range-colors>
	| <range-indexes> "<" <range-indexes>
	| <range-indexes> "\=" <range-indexes>
	| <range-indexes> = <range-indexes>
	| <var-nodes> "\=" <var-nodes>
	| <var-nodes> = <var-nodes>
	| are_constrained("'neq_nodes_nodes_1'", <var-nodes>, <var-nodes>)
	| is_satisfied("'neq_nodes_nodes_1'", <var-nodes>, <var-nodes>)
	| is_violated("'neq_nodes_nodes_1'", <var-nodes>, <var-nodes>)

<move-update> ::= <move-update-atomic>, <move-update>
	| <move-update-atomic>

<move-update-atomic> ::= flip_variable(<var-nodes>, <range-colors>, <range-colors>)
	| flip_variable(<var-nodes-meta>, <range-colors>, <range-colors>)
    | swap_values(<var-nodes>, <var-nodes>)
	| swap_values(<var-nodes-meta>, <var-nodes>)
    | set_value(<var-nodes>, <range-colors>)
	| set_value(<var-nodes-meta>, <range-colors>)
