#!/usr/bin/env swipl
:- use_module(library(noodle)).
:- initialization(main, main).

main([Path]) :- main([Path, write]).

main([Path, Mode]) :-
    read(NQL),
    ndl_query(_{serialize:Path, semantics:Mode, mode:stochastic}, [
            NQL   
        ]).
    