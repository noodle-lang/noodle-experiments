from fitness.base_ff_classes.base_ff import base_ff
from .evaluator import Evaluator
import os
import datetime
import tempfile
import socket

class fitness_function(base_ff):
    """
    Basic fitness function template for writing new fitness functions. This
    basic template inherits from the base fitness function class, which
    contains various checks and balances.
    
    Note that all fitness functions must be implemented as a class.
    
    Note that the class name must be the same as the file name.
    
    Important points to note about base fitness function class from which
    this template inherits:
    
      - Default Fitness values (can be referenced as "self.default_fitness")
        are set to NaN in the base class. While this can be over-written,
        PonyGE2 works best when it can filter solutions by NaN values.
    
      - The standard fitness objective of the base fitness function class is
        to minimise fitness. If the objective is to maximise fitness,
        this can be over-written by setting the flag "maximise = True".
    
    """

    # The base fitness function class is set up to minimise fitness.
    # However, if you wish to maximise fitness values, you only need to
    # change the "maximise" attribute here to True rather than False.
    # Note that if fitness is being minimised, it is not necessary to
    # re-define/overwrite the maximise attribute here, as it already exists
    # in the base fitness function class.
    maximise = True

    def __init__(self):
        """
        All fitness functions which inherit from the bass fitness function
        class must initialise the base class during their own initialisation.
        """
        self.best_score = float('-inf')
        
        temp_file_name = "best_results_{}.log".format(socket.gethostname())
        self.best_result_file = open(temp_file_name, 'w+')
        # Initialise base fitness function class.
        super().__init__()
    
    def evaluate(self, ind, **kwargs):
        """
        Default fitness execution call for all fitness functions. When
        implementing a new fitness function, this is where code should be added
        to evaluate target phenotypes.
        
        There is no need to implement a __call__() method for new fitness
        functions which inherit from the base class; the "evaluate()" function
        provided here allows for this. Implementing a __call__() method for new
        fitness functions will over-write the __call__() method in the base
        class, removing much of the functionality and use of the base class.
                
        :param ind: An individual to be evaluated.
        :param kwargs: Optional extra arguments.
        :return: The fitness of the evaluated individual.
        """
        
        # cwd = os.getcwd()
        evaluator = Evaluator(query=ind.phenotype, cwd= "./fitness/experiment", logging = False) 
        evaluator.evaluate()
        code_style_score = evaluator.code_style_score
        neighborhood_score = evaluator.neighborhood_score
        neighborhood_score_details = evaluator.neighborhood_score_details
        n_weight = 5
        score = (code_style_score + neighborhood_score * n_weight)/(n_weight + 1)

        if self.best_score < score:
          self.best_score = score 
          self.best_result_file.write('{} >> {}:\n- code: {}\n- neighborhood: {}\n- details: {}\n{}\n------\n{}\n==='.format(
                                  datetime.datetime.now(), 
                                  score,
                                  code_style_score,
                                  neighborhood_score,
                                  neighborhood_score_details,
                                  ind.phenotype, 
                                  evaluator.optimized_query
                                  ))
          self.best_result_file.flush()
        return score
