#!/usr/bin/env swipl
:- use_module(library(noodle)).
:- use_module(library(http/json)).

:- initialization(main, main).

main([QueryString]) :-
    term_to_atom(Query, QueryString),
    optimize_query(Query, Stats, OptimizedQuery),
    contains_modifier(OptimizedQuery, Valid),
    create_query_string(OptimizedQuery, OptimizedQueryString),
    Output = output{
        stats: Stats,
        optimized_query: OptimizedQueryString,
        valid: Valid
    },
    current_output(Stream),
    json_write(Stream, Output).

create_query_string(QueryTail, QueryString) :-
    term_to_atom(QueryTail, QueryTailString),
    atomic_list_concat(['(neighbor :- ', QueryTailString, ').'], QueryString).
    
optimize_query(Code, Stats, OptimizedQuery) :-
    Code = (neighbor :- Query),
    conjuct_to_list(Query, QueryList),
    first_pass(QueryList, Stats1, OptimizedQueryList1),
    second_pass(OptimizedQueryList1, Stats2, OptimizedQueryList2),
    third_pass(OptimizedQueryList2, OptimizedQueryList3),
    Stats = Stats1.put_dict(Stats2),
    list_to_conjuct(OptimizedQueryList3, OptimizedQuery).

first_pass(T, Stats, OptimizedQuery) :-
    once(first_pass(T, Stats, [], OptimizedQuery)).

first_pass([], Stats, _, []) :- 
    Stats = results{
        ignored_outputs:0, 
        ignored_outputs_max:0,
        risky_terms:0, 
        all_terms:0, 
        missing_inputs:0, 
        missing_inputs_max:0,
        shadowed:0,
        shadowed_max:0,
        self_loops:0,
        self_loops_max:0,
        meta_terms:0
    }.
first_pass([Term|Rest], Stats, AvailableVars, OptimizedQuery) :-
    missing_ratio(Term, AvailableVars, MissingRatio, MaxMissing),
    shadowing_ratio(Term, AvailableVars, ShadowingRatio, MaxShadowing),
    is_self_loop(Term, SelfLoop, MaxSelfLoop),
    is_risky_term(Term, RiskyTerm),
    ignored_output(Term, Rest, AvailableVars, Ignored, MaxIgnored),
    output_variables(Term, OutputVariables),
    strict_sum(OutputVariables, AvailableVars, NewAvailableVars),
    subquery(Term, SubQuery, SubQueryOutput),
    strict_sum(SubQueryOutput, AvailableVars, SubAvailableVars),
    first_pass(SubQuery, SubStats, SubAvailableVars, OptimizedSubquery),
    first_pass(Rest, NewStats, NewAvailableVars, OptimizedNextQuery),    
    subquery_replace(Term, OptimizedSubquery, OptimizedTerm),
    ((\+ is_meta(Term), prune_term(MissingRatio, MaxMissing,
        SelfLoop, MaxSelfLoop,
        Ignored, MaxIgnored,
        ShadowingRatio, MaxShadowing) -> 
            OptimizedQuery = OptimizedNextQuery)
        ;
            OptimizedQuery = [OptimizedTerm|OptimizedNextQuery]
    ),
    (is_meta(Term) -> MetaTerm = 1 ; MetaTerm = 0),
    MissingInputs is MissingRatio + NewStats.missing_inputs + SubStats.missing_inputs,
    AllInputs is MaxMissing + NewStats.missing_inputs_max + SubStats.missing_inputs_max,
    SelfLoops is SelfLoop + NewStats.self_loops + SubStats.self_loops,
    MaxSelfLoops is MaxSelfLoop + NewStats.self_loops_max + SubStats.self_loops_max,
    Shadowed is ShadowingRatio + NewStats.shadowed + SubStats.shadowed,
    MaxShadowed is MaxShadowing + NewStats.shadowed_max + SubStats.shadowed_max,
    RiskyTerms is RiskyTerm + NewStats.risky_terms + SubStats.risky_terms,
    AllTerms is 1 + NewStats.all_terms + SubStats.all_terms,
    IgnoredOutputs is Ignored + NewStats.ignored_outputs + SubStats.ignored_outputs,
    AllOutputs is MaxIgnored + NewStats.ignored_outputs_max + SubStats.ignored_outputs_max,
    MetaTerms is MetaTerm + NewStats.meta_terms + SubStats.meta_terms,
    Stats = stats{
        missing_inputs: MissingInputs,
        missing_inputs_max: AllInputs,
        self_loops: SelfLoops,
        self_loops_max: MaxSelfLoops,
        shadowed: Shadowed,
        shadowed_max: MaxShadowed,
        risky_terms: RiskyTerms,
        all_terms: AllTerms,
        ignored_outputs: IgnoredOutputs,
        ignored_outputs_max: AllOutputs,
        meta_terms: MetaTerms
    }.

second_pass(T, Stats, OptimizedQuery) :-
    once(second_pass(T, Stats, [], OptimizedQuery)).

second_pass([], Stats, _, []) :- 
    Stats = results{
        effective:0, 
        effective_max:0
    }.

second_pass([Term|Rest], Stats, AvailableVars, OptimizedQuery) :-
    effective_output(Term, Rest, AvailableVars, Effective),
    subquery(Term, SubQuery, SubOutput),
    ( Effective = 1 ->
        output_variables(Term, EffectiveOutput),
        strict_sum(EffectiveOutput, AvailableVars, NewAvailableVars),
        strict_sum(SubOutput, AvailableVars, SubAvailableVars),
        OptimizedQuery = [OptimizedTerm|OptimizedRest]
        ;
        NewAvailableVars = AvailableVars,
        SubAvailableVars = AvailableVars,
        OptimizedQuery = OptimizedRest   
    ),
    second_pass(Rest, NewStats, NewAvailableVars, OptimizedRest),
    second_pass(SubQuery, SubStats, SubAvailableVars, OptimizedSubQuery),
    NewEffective is Effective + NewStats.effective + SubStats.effective,
    NewEffectiveMax is 1 + NewStats.effective_max + SubStats.effective_max,
    Stats = results{effective: NewEffective, effective_max:NewEffectiveMax},
    subquery_replace(Term, OptimizedSubQuery, OptimizedTerm).

third_pass(I,O) :-
    third_pass(I,O,[]).
third_pass([],[],_).
third_pass([Term|Rest], NewRest, AvailableVars) :-
    missing_ratio(Term, AvailableVars, Missing, _),
    Missing > 0, !,
    third_pass(Rest, NewRest, AvailableVars).
third_pass([true|Rest], NewRest, AvailableVars) :-
    !,
    third_pass(Rest, NewRest, AvailableVars).
third_pass([Term|Rest], [NewTerm|NewRest], AvailableVars) :-
    output_variables(Term, Output),
    strict_sum(Output, AvailableVars, NewAvailableVars),
    third_pass(Rest, NewRest, NewAvailableVars),
    subquery(Term, SubQuery, SubQueryOutput),
    strict_sum(SubQueryOutput, AvailableVars, SubAvailableVars),
    third_pass(SubQuery, NewSubQuery, SubAvailableVars),
    subquery_replace(Term, NewSubQuery, NewTerm).

contains_modifier(Query, Result) :-
    conjuct_to_list(Query, QueryList),
    contains_modifier_(QueryList, Result).
contains_modifier_([], 0).
contains_modifier_([Term|_], 1) :-
    Term =.. [F | _],
    is_modifier(F), !.
contains_modifier_([Term|Rest], Modifier) :-
    subquery(Term, SubQuery, _),
    contains_modifier_(Rest, RM),
    contains_modifier_(SubQuery, SM),
    Modifier is max(RM,SM).

prune_term(Missing, PossibleMissing, 
    _, _,
    _, _, 
    _, _) :-
    Missing > 0, PossibleMissing > 0, !.
prune_term(_, _, 
    SelfLoop, PossibleSelfLoop,
    _, _, 
    _, _) :-
    SelfLoop > 0, PossibleSelfLoop > 0, !.
prune_term(_, _, 
    _, _,
    IgnoredOutput, PossibleIgnoredOutput, 
    _, _) :-
    IgnoredOutput > 0, PossibleIgnoredOutput > 0, !.

missing_ratio(Term, _, 0, 0) :-
    required_input(Term, []), !.
missing_ratio(Term, AvailableVars, MissingRatio, 1) :-
    required_input(Term, Required),
    strict_subtract(Required, AvailableVars, Missing),
    length(Missing, MissingSize),
    length(Required, RequiredSize),
    MissingRatio is MissingSize / RequiredSize.

shadowing_ratio(Term, AvailableVars, ShadowingRatio, 1) :-
    Term =.. [F, G | _],
    is_meta(F), !,
    output_variables(G, TermOutput),
    strict_subtract(TermOutput, AvailableVars, TermSourced),
    length(TermOutput, TermOutputSize),
    length(TermSourced, TermSourcedSize),
    ShadowingRatio is 1 - TermSourcedSize/TermOutputSize.
shadowing_ratio(_, _, 0, 0).

ignored_output(Term, _, AvailableVars, 0, 0) :-
    output_variables(Term, Output),
    strict_subtract(Output, AvailableVars, []),
    subquery(Term, _, []),
    !.
ignored_output(Term, Rest, AvailableVars, Ignored, 1) :-
    Term =.. [F | _],
    \+ is_meta(F),
    output_variables(Term, Output), 
    strict_subtract(Output, AvailableVars, RealOutput),
    is_output_ignored(RealOutput, Rest, Ignored).
ignored_output(Term, _, _, Ignored, 1) :-
    Term =.. [F | _],
    is_meta(F),
    subquery(Term, SubQuery, Output), 
    is_output_ignored(Output, SubQuery, Ignored).

is_output_ignored([], _, 0) :- !.
is_output_ignored(_, [], 1).
is_output_ignored(Output, [Term | _], 0) :-
    input_variables(Term, Input),
    strict_intersection(Output, Input), !.
is_output_ignored(Output, [Term | Rest], Ignored) :-
    subquery(Term, SubQuery, _),
    is_output_ignored(Output, SubQuery, SubIgnored),
    is_output_ignored(Output, Rest, RestIgnored),
    min_list([SubIgnored, RestIgnored], Ignored).

effective_output(Term, Rest, AvailableVars, Effective) :-
    guard_tests(AvailableVars, [Term|Rest], [GuardTerm|GuardRest]),
    effective_output_(GuardTerm, GuardRest, AvailableVars, Effective).
effective_output_(Term, _, AvailableVars, 1) :-
    output_variables(Term, Output),
    strict_subtract(Output, AvailableVars, []),
    subquery(Term, _, []),
    !.
effective_output_(Term, Rest, AvailableVars, Effective) :-
    Term =.. [F | _],
    \+ is_meta(F),
    output_variables(Term, Output), 
    strict_subtract(Output, AvailableVars, RealOutput),
    is_output_effective(RealOutput, Rest, Effective).
effective_output_(Term, _, _, Effective) :-
    Term =.. [F | _],
    is_meta(F),
    subquery(Term, SubQuery, Output), 
    is_output_effective(Output, SubQuery, Effective).

is_output_effective([], _, 1) :- !.
is_output_effective(_, [], 0).
is_output_effective(Output, [Term | _], 1) :-
    Term =.. [F | _],
    is_modifier(F),
    input_variables(Term, Input),
    strict_intersection(Output, Input), !.
is_output_effective(Output, [Term | Rest], Effective) :-
    subquery(Term, SubQuery, SubOutput),
    input_variables(Term, Input),
    ( strict_intersection(Output,Input) ->
        output_variables(Term, RestOutput),
        strict_sum(SubOutput, Output, NewSubOutput),
        strict_sum(RestOutput, Output, NewRestOutput)
        ;
        NewSubOutput = Output, 
        NewRestOutput = Output
    ),
    is_output_effective(NewSubOutput, SubQuery, SubEffective),
    is_output_effective(NewRestOutput, Rest, RestEffective),
    max_list([SubEffective, RestEffective], Effective).

guard_tests(_, [], []).
guard_tests(Variables, [Term | Rest], [GuardTerm | GuardRest]) :-
    Term =.. [F | _],
    \+ is_test(F), 
    subquery(Term, SubQuery, _),
    guard_tests(Variables, SubQuery, GuardSubQuery),
    subquery_replace(Term, GuardSubQuery, GuardTerm),
    guard_tests(Variables, Rest, GuardRest).
guard_tests(Variables, [Term | Rest], GuardQuery) :-
    Term =.. [F | _],
    is_test(F),
    input_variables(Term, Input),
    ( strict_intersection(Input, Variables) ->
        GuardQuery = [internal_test_guard(Term) | GuardRest]
        ;
        GuardQuery = [Term | GuardRest]
    ),
    guard_tests(Variables, Rest, GuardRest).
    


required_input(Term, Required) :-
    input_variables(Term, Input),
    output_variables(Term, Output),
    strict_subtract(Input, Output, Required).

output_variables(Term, Output) :-
    output_variables_(Term, MOutput),
    sort(MOutput, Output).
output_variables_(Term, Output) :-
    Term =.. [variable, _ | Output].
output_variables_(Term, Output) :-
    Term =.. [value, _ | Output].
output_variables_(Term, Output) :-
    Term =.. [constraint, _ | Output].
output_variables_(Term, [Output]) :-
    Term =.. [is, Output | _].
output_variables_(Term, Output) :-
    Term =.. [get_value, _ | Output].
output_variables_(Term, Output) :-
    Term =.. [range_element, _ | Output].
output_variables_(Term, []) :-
    Term =.. [F | _],
    is_test(F).
output_variables_(Term, []) :-
    Term =.. [F | _],
    is_modifier(F).
output_variables_(Term, []) :-
    Term =.. [for_each | _].
output_variables_(Term, []) :-
    Term =.. [F | _],
    is_walk(F).
output_variables_(Term, []) :-
    Term =.. [F | _],
    is_iterator(F).
output_variables_(Term, Output) :-
    Term =.. [-, T1, T2],
    input_variables(T1, O1),
    input_variables(T2, O2),
    append(O1, O2, Output).   

input_variables(Term, Input) :-
    input_variables_(Term, MInput),
    sort(MInput, Input).
input_variables_(Term, Input) :-
    Term =.. [variable, _ | Input].
input_variables_(Term, Input) :-
    Term =.. [value, _ | Input].
input_variables_(Term, Input) :-
    Term =.. [constraint, _ | Input].
input_variables_(Term, Input) :-
    Term =.. [is, _ | Input].
input_variables_(Term, [Input]) :-
    Term =.. [get_value, Input | _].
input_variables_(Term, []) :-
    Term =.. [range_element | _].
input_variables_(Term, Input) :-
    Term =.. [F | Input],
    is_test(F), 
    \+ is_constraint_test(F).
input_variables_(Term, Input) :-
    Term =.. [F, _ | Input],
    is_constraint_test(F).
input_variables_(Term, Input) :-
    Term =.. [F | Input],
    is_modifier(F).
input_variables_(Term, []) :-
    Term =.. [for_each | _].
input_variables_(Term, [Input]) :-
    Term =.. [F, _, Input, _],
    is_walk(F).
input_variables_(Term, [Input]) :-
    Term =.. [F, _, Input, _],
    is_iterator(F).
input_variables_(Term, Input) :-
    Term =.. [internal_test_guard, Test],
    input_variables(Test, Input).
input_variables_(Term, Input) :-
    Term =.. [-, T1, T2],
    input_variables(T1, I1),
    input_variables(T2, I2),
    append(I1, I2, Input).    
subquery(Term, SubQuery, SubQueryOutput) :-
    Term =.. [for_each, G, S], !,
    output_variables(G, SubQueryOutput),
    conjuct_to_list(S, SubQuery).
subquery(Term, SubQuery, SubQueryOutput) :-
    Term =.. [F, G, _, S],
    is_walk(F), !,
    output_variables(G, SubQueryOutput),
    conjuct_to_list(S, SubQuery).
subquery(Term, SubQuery, SubQueryOutput) :-
    Term =.. [F, G, _, S],
    is_iterator(F), !,
    output_variables(G, SubQueryOutput),
    conjuct_to_list(S, SubQuery).
subquery(_, [], []).

subquery_replace(Term, NewSubQueryList, NewTerm) :-
    list_to_conjuct(NewSubQueryList, NewSubQuery),
    Term =.. [for_each, G, _], !,
    NewTerm =.. [for_each, G, NewSubQuery].
subquery_replace(Term, NewSubQueryList, NewTerm) :-
    list_to_conjuct(NewSubQueryList, NewSubQuery),
    Term =.. [F, G, S, _],
    is_walk(F), !,
    NewTerm =.. [F, G, S, NewSubQuery].
subquery_replace(Term, NewSubQueryList, NewTerm) :-
    list_to_conjuct(NewSubQueryList, NewSubQuery),
    Term =.. [F, G, S, _],
    is_iterator(F), !,
    NewTerm =.. [F, G, S, NewSubQuery].
subquery_replace(Term, _, Term).

is_meta(for_each).
is_meta(X) :- is_walk(X).
is_meta(X) :- is_iterator(X).
is_walk(walk_over).
is_walk(walk_over_inversed).
is_iterator(iterate).
is_iterator(iterate_backward).
is_modifier(set_value).
is_modifier(swap_values).
is_modifier(flip_variable).
is_modifier(internal_test_guard).
is_constraint_test(is_satisfied).
is_constraint_test(is_violated).
is_constraint_test(are_constrained).

is_test(=).
is_test(\=).
is_test(<).
is_test(=<).
is_test(>).
is_test(>=).
is_test(true).
is_test(X) :- is_constraint_test(X).

extract_vars(Term, Vars) :-
    Term =.. [_ | Args],
    include(var, Args, Vars).

risky_functor(get_value).
risky_functor(set_value).
risky_functor(swap_values).
risky_functor(flip_variable).
risky_functor(F) :- is_test(F).

is_risky_term(Term, 1) :-
    Term =.. [F | _],
    risky_functor(F),
    !.
is_risky_term(_, 0).
    
loopy_functor(constraint).
loopy_functor(are_constrained).
loopy_functor(flip_variable).
loopy_functor(swap_values).
loopy_functor(is_satisfied).
loopy_functor(is_violated).
loopy_functor(for_each).
loopy_functor(walk_over).
loopy_functor(walk_over_inversed).
loopy_functor(iterate).
loopy_functor(iterate_backward).
loopy_functor(=).
loopy_functor(\=).
loopy_functor(-).

is_self_loop(Term, SL, MSL) :-
    once(is_self_loop_(Term, SL, MSL)).
is_self_loop_(Term, SelfLoop, MaxSelfLoop) :-
    Term =.. [for_each, G, _], !,
    is_self_loop(G, SelfLoop, MaxSelfLoop).
is_self_loop_(Term, SelfLoop, MaxSelfLoop) :-
    Term =.. [F, G, _, _],
    is_walk(F),
    is_self_loop(G, SelfLoop, MaxSelfLoop).
is_self_loop_(Term, SelfLoop, MaxSelfLoop) :-
    Term =.. [F, G, _, _],
    is_iterator(F),
    is_self_loop(G, SelfLoop, MaxSelfLoop).
is_self_loop_(Term, SelfLoop, 1) :-
    Term =.. [F | _],
    loopy_functor(F),
    extract_vars(Term, Vars),
    (not_distinct(Vars) -> SelfLoop = 1; SelfLoop = 0).
is_self_loop_(_, 0, 0).

not_distinct(Variables) :-
    sort(Variables, S),
    length(Variables, VL),
    length(S, SL),
    SL < VL.

strict_member(X, [Y|_]) :-
    X == Y, !.
strict_member(X, [_|T]) :-
    strict_member(X, T).

strict_subtract([], _, []).
strict_subtract([X|XR], Y, R) :-
    strict_member(X, Y), !,
    strict_subtract(XR, Y, R).
strict_subtract([X|XR], Y, [X|R]) :-
    strict_subtract(XR, Y, R).

strict_intersection([X|_], Y) :-
    strict_member(X,Y), !.
strict_intersection([_|R], Y) :-
    strict_intersection(R,Y).

strict_sum([], Y, Y).
strict_sum([X|R], Y, NR) :-
    strict_member(X,Y), !,
    strict_sum(R, Y, NR).
strict_sum([X|R], Y, [X|NR]) :-
    strict_sum(R, Y, NR).

:- begin_tests(query_analyzer).

test(guard_empty) :- guard_tests([_, _], [], []).
% guard_tests(_, [], []).
% guard_tests(Variables, [Term | Rest], [GuardTerm | GuardRest]) :-
%     Term =.. [F | _],
%     \+ is_test(F), 
%     subquery(Term, SubQuery, _),
%     guard_tests(Variables, SubQuery, GuardSubQuery),
%     subquery_replace(Term, GuardSubQuery, GuardTerm),
%     guard_tests(Variables, Rest, GuardRest).
% guard_tests(Variables, [Term | Rest], GuardQuery) :-
%     Term =.. [F | Input],
%     is_test(F),
%     guard_tests(Variables, Rest, GuardRest),
%     ( strict_subtract(Input, Variables, []) ->
%         GuardQuery = []
%         ;
%         GuardQuery = [internal_test_guard(Term) | GuardRest]
%     ).


:- end_tests(query_analyzer).
