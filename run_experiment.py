#!/usr/bin/env python3

from typing import Dict, List, Tuple
from sultan.api import Sultan 
from pathlib import Path
from glob import glob
import numpy as np
import sys 
import argparse
import tempfile
import os
import sys
import importlib
import time
import subprocess
import json 
import time
import re
import socket

def arg_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description='NDL experiment runner')
    parser.add_argument('experiment', default='tsp', help='name of the experiment')
    parser.add_argument('--ndl', action='store_true', help='do not read the minizinc files')
    parser.add_argument('--bnf', action='store_true', help='do not create the grammar automatically')
    return parser

def experiment_dir_name(name) :
    timestr = time.strftime("%Y%m%d-%H%M%S")    
    return "experiments/{}_{}_{}".format(name, timestr, socket.gethostname())

def create_dir(name):
    with Sultan.load() as sh:
        sh.mkdir('-p {}'.format(experiment_dir)).and_()\
        .cp('-r PonyGE2 {}'.format(experiment_dir)).and_()\
        .cp('-r oscar-ndl {}'.format(experiment_dir)).and_()\
        .cp(' -r problems/{}/oscar-model {}/oscar-ndl/src/main/scala/Experiment'.format(experiment, experiment_dir)).and_()\
        .cp('problems/{}/oscar-ndl-domains/*.ndl {}/oscar-ndl/ndl'.format(experiment, experiment_dir)).and_()\
        .cp('-r {} {}'.format('problems/template/fitness', fitness_dir)).and_()\
        .cp('-r {}/* {}'.format(problem_fitness_dir, fitness_dir)).and_()\
        .mkdir(grammar_dir).and_()\
        .cp('-r {}/parameters {}/PonyGE2'.format('problems/template', experiment_dir))\
        .cp('-r {}/parameters/* {}/PonyGE2/parameters'.format(problem_base_dir, experiment_dir))\
        .run()

def all_mzn2ndl(experiment):
    from glob import glob
    minizinc_files = glob('problems/{}/minizinc/*.mzn'.format(experiment))
    instance_dir = "{}/instances".format(fitness_dir)
    for mzn_file in minizinc_files:
        basename = Path(mzn_file).stem
        mzn2ndl(mzn_file, instance_dir, basename)
        mzn2sol(mzn_file, instance_dir, basename)

def mzn2ndl(input, output_dir, output_name):
    with Sultan.load() as sh:
        sh.swipl('mzn2ndl/mzn2ndl.pl {}'.format(input)).redirect(
            "{}/{}.pl".format(output_dir, output_name),
            append=False,
            stdout=True,
            stderr=False).and_()\
        .cd(output_dir)\
        .swipl('''-f {}.pl -g "build('{}')."'''.format(output_name, output_name))\
        .rm("{}.pl".format(output_name))\
        .run()

def ndl2bnf(experiment, use_provided_grammar):
    if use_provided_grammar:
        with Sultan.load() as sh:
            sh.cp('problems/{}/grammars/grammar.bnf {}/grammar.bnf'.format(experiment, grammar_dir)).run()
    else:
        from glob import glob
        ndl_file = glob('{}/instances/*.ndl'.format(fitness_dir))[0]
        with Sultan.load() as sh:
            sh.swipl('ndl2bnf/ndl2bnf.pl {}'.format(ndl_file)).redirect(
                "{}/grammar.bnf".format(grammar_dir),
                append=False,
                stdout=True,
                stderr=False).run()

def mzn2sol(input, output_dir, output_name):
    def seed():
        return int(time.time() * 10000)

    def name_for_index(index: Tuple[int], name: str, mins: List[int]):
        out_index = [str(x + y) for x, y in zip(index, mins)]
        return "{}({})".format(name, ','.join(out_index))

    def parse_output(output:str):
        r = re.compile(r'''([a-zA-Z_0-9]+)\s*=\s*[a-zA-Z_0-9]+\(([^\[]+),\s*\[([^\]]*)\]\);''')
        parsed_outputs = []
        for m in r.findall(output):
            name = m[0]
            if (name[0].isupper()):
                name = "'{}'".format(name)
            ranges = [tuple(int(d) for d in r.strip().split('..')) for r in m[1].split(',')]
            values = [int(v.strip()) for v in m[2].split(',')]
            mins = [r[0] for r in ranges]
            sizes = tuple([y - x + 1 for (x,y) in ranges])
            valArray = np.array(values)
            valArray = np.resize(valArray, sizes)
            for i, v in np.ndenumerate(valArray):
                i_name = name_for_index(i, name, mins)
                parsed_outputs.append("{}-{}".format(i_name, v))
        return parsed_outputs

    solutions = []
    with tempfile.TemporaryDirectory() as tmpdirname, Sultan.load() as sh:
        sat_path = "{}/{}_sat.mzn".format(tmpdirname, output_name)
        r = re.compile(r'''(minimize|maximize) [^;]*''')
        with open(input) as opt_file, open(sat_path, 'w') as sat_file:
            model = opt_file.read()
            sat_model = r.sub("satisfy", model)
            sat_file.write(sat_model)
        sh.minizinc("-c -o {}/{} --solver Gecode --no-output-ozn {}".format(tmpdirname, output_name, sat_path)).run()
        for i in range(3):
            output = sh.fzn__gecode("-r {} {}/{}".format(seed(), tmpdirname, output_name)).run().stdout[0]
            solutions.append("solution({},[{}]).".format(i, ','.join(parse_output(output))))
    
    with open("{}/{}.nsl".format(output_dir, output_name), 'w') as o:
        o.write("\n".join(solutions))

def run_synthesis(workdir):
    with Sultan.load(cwd=workdir) as sh:
        sh.python('ponyge.py --parameters default.txt').redirect(
            "../../synthesis_output.txt",
            append=False,
            stdout=True,
            stderr=False).run()

def extract_synthesis_results(resultsdir):
    last_results = glob("{}/*".format(resultsdir))[0]
    best_results = "{}/best.txt".format(last_results)
    with open(best_results) as f:
        return f.readlines()[4]

def compile_query(operator, fitness_dir, output_path):
    from glob import glob
    domain = glob('{}/instances/*.ndl'.format(fitness_dir))[0]
    with Sultan.load() as sh:
        output = sh.swipl("{}/query-analyzer.pl \"{}\"".format(fitness_dir, operator[0:-1])).run()
        optimized_query = json.loads(str(output))["optimized_query"]
        sh.echo('"{}"'.format(optimized_query)).pipe().swipl("-f {} {}/query-compiler.pl {}".format(domain, fitness_dir, output_path)).run()
        
def run_ndl_server(experiment_dir):
    process = subprocess.Popen(['{}/oscar-ndl/ndl/run_in_bg.sh'.format(experiment_dir)],
                     stdout=subprocess.PIPE, 
                     stderr=subprocess.PIPE)
    return process

def kill(process):
    process.kill()

def run_synthesized_oscar(experiment_dir):
    process = run_ndl_server(experiment_dir)
    with Sultan.load(cwd = '{}/oscar-ndl'.format(experiment_dir)) as sh:
        result = '\n'.join(sh.sh('./gradlew -q runSynthesized').run().stdout)
    kill(process)
    return result

def run_basic_oscar(experiment_dir):
    with Sultan.load(cwd = '{}/oscar-ndl'.format(experiment_dir)) as sh:
        result = '\n'.join(sh.sh('./gradlew -q runBasic').run().stdout)
    return result
        

if __name__ == "__main__":
    arg_parser = arg_parser()
    args = arg_parser.parse_args()
    experiment = args.experiment
    experiment_dir = experiment_dir_name(experiment)
    use_provided_grammar = args.bnf
    synthesis_dir = "{}/PonyGE2/src".format(experiment_dir)
    fitness_dir = '{}/fitness/experiment'.format(synthesis_dir)
    grammar_dir = '{}/PonyGE2/grammars'.format(experiment_dir)
    synthesis_results_dir = "{}/PonyGE2/results".format(experiment_dir)
    problem_base_dir = 'problems/{}'.format(experiment)
    problem_fitness_dir = '{}/fitness'.format(problem_base_dir)

    print("-> creating dedicated directory ({})...".format(experiment_dir))
    create_dir(experiment_dir)
    print("-> translating MiniZinc to Noodle models...")
    all_mzn2ndl(experiment)
    print("-> generating a BNF grammar for the specified problem")
    ndl2bnf(experiment, use_provided_grammar)
    print("-> generating the neighborhood (stdout redirected to {}/synthesis_output.txt)...".format(experiment_dir))
    run_synthesis(synthesis_dir)
    neighborhood = extract_synthesis_results(synthesis_results_dir).strip()
    print('-> synthesized neighborhood: {}'.format(neighborhood))
    compile_query(neighborhood, fitness_dir, '{}/oscar-ndl/ndl/query'.format(experiment_dir))
    print('-> running model with synthesized operator...')
    print(run_synthesized_oscar(experiment_dir))
    print('-> running model with basic operator...')
    print(run_basic_oscar(experiment_dir))