package NDL

import java.io.{BufferedReader, BufferedWriter, InputStreamReader, OutputStreamWriter, PrintWriter}
import java.net.Socket

import oscar.cbls.core.computation.CBLSIntVar
import oscar.cbls.core.search.{Best, CompositeMove, EasyNeighborhoodMultiLevel, First, LoopBehavior}
import oscar.cbls.lib.search.neighborhoods.AssignMove

import sys.process._
import resource._

object NDLConnection {


}

case class NDLNeighborhood(vars:Array[CBLSIntVar], queryName:String, instanceName:String, in:BufferedReader, out:PrintWriter, loopBehavior: LoopBehavior = Best(() => Int.MaxValue), name:String = "NDLNeighborhood")
  extends EasyNeighborhoodMultiLevel[CompositeMove](neighborhoodName = name) {

  val changesRegexp = "([A-Za-z][A-Za-z0-9_]*\\([^\\)]+\\))-([0-9]+)".r
  val nameIndex = vars map (v => v.name -> v) toMap
  var changes:List[(CBLSIntVar, Int)] = _

  override def exploreNeighborhood(): Unit = {
      loadProblem(in, out)
      loadState(in, out)
      val neighbors = new NeighborhoodIterable(in,out)
      val (neighborsIterator, notify) = loopBehavior.toIterator(neighbors)

      while (neighborsIterator.hasNext) {
        changes = neighborsIterator.next()
        val newObj = obj.assignVal(changes)
        if (evaluateCurrentMoveObjTrueIfSomethingFound(newObj)) {
          notify()
        }
      }
      endNeighborhood(in, out)
  }

  override def instantiateCurrentMove(newObj: Int): CompositeMove = {
    val moves: List[AssignMove] = changes.map { case(variable,value) => AssignMove(variable,value,0,Int.MaxValue,name) }
    CompositeMove(moves, newObj, name)
  }

  def serializeVariables: String = {
    vars.map(v => s"${v.name}-${v.value}").mkString("[",",","]")
  }

  def deserializeChanges(changes: String):List[(CBLSIntVar, Int)] = {
    
    try { 
      changesRegexp.findAllMatchIn(changes)
      .map{ ch => (ch.group(1),ch.group(2)) }
      .map{ case (name,textValue) => (nameIndex(name), textValue.toInt) }
      .toList
    } 
    catch {
      case _: Throwable => List[(CBLSIntVar, Int)]()
    }
  }

  def loadProblem(in: BufferedReader, out:PrintWriter) = {
//    println(s"Loading problem: load($instanceName).")
    out.println(s"load($queryName, $instanceName).")
    val reply = in.readLine()
//    println(s"Got reply $reply")
  }

  def loadState(in: BufferedReader, out:PrintWriter) = {
    val state = serializeVariables
    out.println(s"neighbors($state).")
    val reply = in.readLine()
//    println(s"Loading state, got reply $reply")
  }

  def endNeighborhood(in: BufferedReader, out:PrintWriter) = {
//    println("Ending neighborhood")
    out.println("end.")
  }

  private class NeighborhoodIterator(in:BufferedReader, out:PrintWriter) extends
    Iterator[List[(CBLSIntVar, Int)]] {
    var nextNeighbor: List[(CBLSIntVar, Int)] = null

    override def hasNext: Boolean = {
      nextNeighbor = getNeighbor
      nextNeighbor != null
    }

    override def next(): List[(CBLSIntVar, Int)] = { nextNeighbor }

    def getNeighbor:List[(CBLSIntVar, Int)] = {
//      println(s"Asked for the next neighbor")
      out.println("next.")
      val reply = in.readLine()
//      println(s"Got reply $reply")
      if (reply == "end.") {
        return null
      }
      deserializeChanges(reply)
    }
  }

  private class NeighborhoodIterable(reader: BufferedReader, writer: PrintWriter) extends Iterable[List[(CBLSIntVar, Int)]] {
    override def iterator: Iterator[List[(CBLSIntVar, Int)]] = new NeighborhoodIterator(reader, writer)
  }
}

object NDLNeighborhood {
  def withConnection(neighborhoodBuilder:(BufferedReader, PrintWriter) => Unit, port:Int = 3030, host:String = "localhost"): Unit = {
    for {connection <- managed(new Socket(host, port))
         outStream <- managed(connection.getOutputStream)
         out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(outStream)), true)
         inStream <- managed(connection.getInputStream)
         in = new BufferedReader(new InputStreamReader(inStream))
         } {
      neighborhoodBuilder(in, out)
    }
  }
}
