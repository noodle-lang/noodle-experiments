package NDL

import oscar.cbls.Objective
import oscar.cbls.core.search.{MoveFound, Neighborhood, NeighborhoodCombinator, NoMoveFound, SearchResult}

import scala.collection.mutable.ListBuffer

/**
 * collects statistics about the run time and progress achieved by neighborhood a
 * they can be obtained by querying this object with method toString
 * or globally on the whole neighborhood using the method statistics
 * WARNING: do not use this inside an AndThen,
 *          since the objective function is instrumented by this combinator, so the statistics will be counter-intuitive
 *
 * @param a
 * @param ignoreInitialObj
 */
case class NDLProfile(a:Neighborhood,ignoreInitialObj:Boolean = false) extends NeighborhoodCombinator(a){

  var nbCalls = 0
  var nbFound = 0
  var totalGain:Double = 0
  var totalTimeSpentMoveFound:Long = 0
  var totalTimeSpentNoMoveFound:Long = 0
  var iterations:ListBuffer[Int] = ListBuffer()

  def totalTimeSpent = totalTimeSpentMoveFound + totalTimeSpentNoMoveFound

  override def resetStatistics(){
    resetThisStatistics()
    super.resetStatistics()
  }

  def resetThisStatistics() {
    nbCalls = 0
    nbFound = 0
    totalGain = 0
    totalTimeSpentMoveFound = 0
    totalTimeSpentNoMoveFound = 0
    iterations = ListBuffer()
  }

  /**
   * the method that returns a move from the neighborhood.
   * The returned move should typically be accepted by the acceptance criterion over the objective function.
   * Some neighborhoods are actually jumps, so that they might violate this basic rule however.
   *
   * @param obj the objective function. notice that it is actually a function. if you have an [[oscar.cbls.core.objective.Objective]] there is an implicit conversion available
   * @param acceptanceCriterion
   * @return
   */
  override def getMove(obj: Objective, initialObj:Int, acceptanceCriterion: (Int, Int) => Boolean): SearchResult = {

    nbCalls += 1
    val startTime = System.nanoTime()

    a.getMove(obj, initialObj:Int, acceptanceCriterion) match {
      case NoMoveFound =>
        totalTimeSpentNoMoveFound += (System.nanoTime() - startTime) / 1000000
        iterations += initialObj
        NoMoveFound
      case m: MoveFound =>
        totalTimeSpentMoveFound += (System.nanoTime() - startTime) / 1000000
        nbFound += 1
        iterations += m.objAfter
        if (!ignoreInitialObj || nbCalls > 1) totalGain += initialObj - m.objAfter
        m
    }
  }

  def gainPerCall:String = if(nbCalls ==0) "NA" else "" + (totalGain / nbCalls).toInt
  def callDuration:String = if(nbCalls == 0 ) "NA" else "" + (totalTimeSpent / nbCalls).toInt
  //gain in obj/s
  def slope:String = if(totalTimeSpent == 0) "NA" else "" + (1000 * totalGain.toDouble / totalTimeSpent.toDouble).toInt

  def avgTimeSpendNoMove:String = if(nbCalls - nbFound == 0) "NA" else "" + (totalTimeSpentNoMoveFound / (nbCalls - nbFound))
  def avgTimeSpendMove:String = if(nbFound == 0) "NA" else "" + (totalTimeSpentMoveFound / nbFound)
  def waistedTime:String = if(nbCalls - nbFound == 0) "NA" else "" + (totalTimeSpentNoMoveFound / (nbCalls - nbFound))

  override def collectProfilingStatistics: List[String] =
    collectThisProfileStatistics :: super.collectProfilingStatistics

  def collectThisProfileStatistics:String =
    padToLength("" + a,31) + " " +
      padToLength("" + nbCalls,6) + " " +
      padToLength("" + nbFound,6) + " " +
      padToLength("" + totalGain.toInt,8) + " " +
      padToLength("" + totalTimeSpent,12) + " " +
      padToLength("" + gainPerCall,8) + " " +
      padToLength("" + callDuration,12)+ " " +
      padToLength("" + slope,11)+ " " +
      padToLength("" + avgTimeSpendNoMove,13)+ " " +
      padToLength("" + avgTimeSpendMove,12)+ " " +
      totalTimeSpentNoMoveFound

  def collectThisProfileIterations:Array[Int] = iterations.toArray

  private def padToLength(s: String, l: Int) = {
    val extended = s + nStrings(l+1, " ")
    val nextchar = extended.substring(l+1, l+1)
    if(nextchar equals " "){
      extended.substring(0, l-1) + "§"
    }else{
      extended.substring(0, l)
    }
  }
  private def nStrings(n: Int, s: String): String = if (n <= 0) "" else s + nStrings(n - 1, s)

  //  override def toString: String = "Statistics(" + a + " nbCalls:" + nbCalls + " nbFound:" + nbFound + " totalGain:" + totalGain + " totalTimeSpent " + totalTimeSpent + " ms timeSpendWithMove:" + totalTimeSpentMoveFound + " ms totalTimeSpentNoMoveFound " + totalTimeSpentNoMoveFound + " ms)"
  override def toString: String = "Profile(" + a + ")"

  def slopeOrZero:Int = if(totalTimeSpent == 0) 0 else ((100 * totalGain) / totalTimeSpent).toInt

  def slopeForCombinators(defaultIfNoCall:Int = Int.MaxValue):Int =  if(totalTimeSpent == 0) defaultIfNoCall else ((1000 * totalGain) / totalTimeSpent).toInt
}

object NDLProfile{
  private def padToLength(s: String, l: Int) = (s + nStrings(l, " ")).substring(0, l)
  private def nStrings(n: Int, s: String): String = if (n <= 0) "" else s + nStrings(n - 1, s)
  def statisticsHeader: String = padToLength("Neighborhood",30) + "  calls  found  sumGain  sumTime(ms)  avgGain  avgTime(ms)  slope(-/s)  avgTimeNoMove avgTimeMove  wastedTime"
  def selectedStatisticInfo(i:Iterable[NDLProfile]) = {
    (statisticsHeader :: i.toList.map(_.collectThisProfileStatistics)).mkString("\n")
  }
}
