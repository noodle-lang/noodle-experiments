#!/usr/bin/env swipl

:- use_module(library(noodle)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).

:- initialization(main, main).

:- dynamic loaded/0.

main([Port]) :-
    set_prolog_flag(stack_limit,  12 000 000 000),
    atom_to_term(Port, PortTerm, []),
    tcp_socket(Socket),
    tcp_bind(Socket, localhost:PortTerm),
    tcp_listen(Socket, 5),
    tcp_open_socket(Socket, AcceptFd, _),
    dispatch(AcceptFd).

dispatch(AcceptFd) :-
    tcp_accept(AcceptFd, Socket, Peer),
    process_client(Socket, Peer),
    dispatch(AcceptFd).

process_client(Socket, _Peer) :-
    setup_call_cleanup(
        tcp_open_socket(Socket, StreamPair),
        handle_service(StreamPair),
        close(StreamPair)
    ).

handle_service(Streams) :-
    set_stream(Streams,timeout(3600)),
    service_loop(Streams).

service_loop(Streams) :-
    repeat(),
    read(Streams, Command),
    once(handle(Command, Streams, Continue)),
    (Continue = true ->
        false
    ;
        true
    ).
handle(end, _, true).
handle(exit, _, false) :-
    writeln("exit").
handle(end_of_file, _, false) :-
    writeln("disconnect").
handle(load(QueryName, ProblemName), Streams, true) :-
    load_problem(QueryName, ProblemName),
    writeln(Streams, "ok."),
    flush_output(Streams).
handle(neighbors(ListState), Streams, true) :-
    ndl_solution(ListState, State),
    writeln(Streams, "ok."),
    flush_output(Streams),
    (read(Streams, next) -> true; fail),
    handle_neighborhood(State, Streams).
handle(Cmd, Streams, true) :-
    format("Got other ~w\n", [Cmd]),
    format(Streams, 'Command ~w is not supported\n', [Cmd]),
    flush_output(Streams).
handle_neighborhood(State, Streams) :-
    neighbor(State, Neighbor),
    ndl_diff(State, Neighbor, Diff),
    writeln(Streams, Diff),
    flush_output(Streams),
    (read(Streams, next) -> fail; true).
handle_neighborhood(_, Streams) :-
    writeln(Streams, "end."),
    flush_output(Streams).
    
load_problem(_) :-
    loaded, !.
load_problem(Problem, Name) :-
    consult_domain(Name),
    consult_query(Problem),
    assert(loaded).

    
consult_domain(Name) :-
    atomic_list_concat([Name,'.ndl'], Domain),
    consult(Domain).

consult_query(Name) :-
    atomic_list_concat([Name,'.nql'], Query),
    consult(Query).