:- module(mzn_model, [
    create_mzn_model/3,
    op(1000, yfx, 'to')
]).

create_mzn_model(model(Items), model{symbols:Symbols, constraints:Constraints}, Errors) :-
    gather_symbols(Items, Symbols, SErrors),
    gather_constraints(Items, Constraints, CErrors),
    append(SErrors, CErrors, Errors).

gather_symbols([], ST, []) :- 
    empty_assoc(ST).
gather_symbols([I|T], SymbolTable, Errors) :-
    I = item(item_var_decl(Id, ValueExpr, _)),
    symbol_is_parameter(Id), !,
    ( ValueExpr = empty ->
        format(atom(E), "- uninitialized parameter ~w", [Id]),
        gather_symbols(T, SymbolTable, TErrors),
        Errors = [E | TErrors] 
        ;
        symbol_from_id(Id, Symbol, IErrors),
        translate_expression(ValueExpr, Value, VErrors),
        list_to_assoc([Symbol-Value], IST),
        gather_symbols(T, TSymbols, TErrors),
        merge_symbol_tables(IST, TSymbols, SymbolTable, SErrors),
        append([IErrors, TErrors, VErrors, SErrors], Errors)
    ).
gather_symbols([I|T], SymbolTable, Errors) :-
    I = item(item_var_decl(Id, _, _)),
    symbol_is_variable(Id), !,
    symbol_from_id(Id, Symbol, IErrors),
    list_to_assoc([Symbol-_], IST),
    gather_symbols(T, TSymbols, TErrors),
    merge_symbol_tables(IST, TSymbols, SymbolTable, SErrors),
    append([IErrors, TErrors, SErrors], Errors).
gather_symbols([_|T], SymbolTable, Errors) :-
    gather_symbols(T, SymbolTable, Errors).
    
symbol_from_id(element(ident(ID), type(_, Type)), Symbol, Errors) :-
    translate_type(Type, SymbolType-Mode, Errors), !,
    translate_ident(ID, TID),
    Symbol = symbol{ id: TID, mode: Mode, type: SymbolType }.
symbol_from_id(element(ident(ID), type(Mode, Type)), Symbol, Errors) :-
    translate_type(Type, SymbolType, Errors), !,
    translate_ident(ID, TID),
    Symbol = symbol{ id: TID, mode: Mode, type: SymbolType }.

translate_ident(ID, TID) :-
    atomic_list_concat(['\'', ID, '\''], TID).
    
translate_type(int, int, []).
translate_type(range(StartExpr, EndExpr), range(Start, End), Errors) :-
    translate_expression(StartExpr, Start, SE),
    translate_expression(EndExpr, End, EE),
    append(SE, EE, Errors).
translate_type(ident(ID), TID, []) :- 
    translate_ident(ID, TID).
translate_type(set_type(Type), set_type(TType), Errors) :-
    translate_type(Type, TType, Errors).
translate_type(array_type(Index, type(Mode, Type)), array_type(SymbolIndex, TType) - Mode, Errors) :-
    translate_type(Type, TType, TErrors),
    translate_array_index(Index, SymbolIndex, AErrors),
    append(TErrors, AErrors, Errors).

translate_array_index([],[],[]).
translate_array_index([type(par, I)|T], [SI|ST], Errors) :-
    translate_type(I, SI, IE),
    translate_array_index(T, ST, TE),
    append(IE, TE, Errors).

translate_array_literal(array(Exprs), array(Items), Errors) :-
    translate_array_literal_contents(Exprs, Items, Errors).
translate_array_literal_contents([], [], []).
translate_array_literal_contents([E|TE], [I|TI], Errors) :-
    translate_expression(E, I, EE),
    translate_array_literal_contents(TE, TI, TErrors),
    append(EE, TErrors, Errors).


symbol_is_variable(element(_, type(var, _))).
symbol_is_variable(element(_,type(par,array_type(_,type(var,_))))).
symbol_is_parameter(element(_, type(par, Type))) :-
    Type \= array_type(_,type(var,_)).

gather_constraints([], [], []).
gather_constraints([I|T], [C|Cs], Errors) :-
    I = item(item_constraint(Expr, _)),
    translate_constraint_expression(Expr, C, E),
    gather_constraints(T, Cs, TErrors),
    append(E, TErrors, Errors).
gather_constraints([_|T], Cs, Errors) :-
    gather_constraints(T, Cs, Errors).

translate_constraint_expression(Expression, Item, Errors) :-
    once(translate_top_constraint_expression(Expression, Item, Errors)), !.
translate_constraint_expression(Expr, _, [E]) :-
    format(atom(E), "- unsupported constraint expression ~w", [Expr]).

translate_top_constraint_expression(Expr, Item, Errors) :-
    unpack_top_expression(Expr, TopElements),
    translate_top_elements(TopElements, [call(CName, CArgs)], [], Errors),
    is_constraint(CName),
    Item =.. [constraint, CName | CArgs].
translate_top_constraint_expression(Expr, ItemTerm, Errors) :-
    unpack_top_expression(Expr, TopElements),
    translate_top_elements(TopElements, [Item], [], Errors),
    translate_expressions_list_to_term([Item], ItemTerm).
translate_top_constraint_expression(Expr, Item, Errors) :-
    unpack_top_expression(Expr, TopElements),
    translate_top_elements(TopElements, Left, [Op | Rest], LErrors),
    translate_expressions_list_to_term(Left, LeftItem),
    is_constraint(Op),
    translate_top_elements(Rest, Right, [], RErrors),
    translate_expressions_list_to_term(Right, RightItem),
    Item =.. [constraint, Op, LeftItem, RightItem],
    append(LErrors, RErrors, Errors).


translate_expression(Expression, Item, Errors) :-
    once(translate_top_expression(Expression, Item, [], Errors)), !.
translate_expression(Expr, _, [E]) :-
    format(atom(E), "- unsupported expression ~w", [Expr]).

translate_top_expression(Expr, Item, Rest, Errors) :-
    unpack_top_expression(Expr, TopElements),
    translate_top_elements(TopElements, Items, Rest, Errors),
    translate_expressions_list_to_term(Items, Item).

translate_top_elements([], [], [], []).
translate_top_elements([H|T], [], [I|T], []) :-
    translate_comp_operator(H, I), !.
translate_top_elements([H|T], [], [I|T], [E]) :-
    translate_logical_operator(H, I, _, _), !,
    format(atom(E), "- constraint composition is not supported yet: ~w", [H]).
translate_top_elements([H|T], [I|Is], Rest, Errors) :-
    is_expression_operator(H), !,
    translate_operator(H, I, E),
    translate_top_elements(T, Is, Rest, Es),
    append(E, Es, Errors).
translate_top_elements([H|T], [I|Is], Rest, Errors) :-
    translate_atom_expression(H, I, E),
    translate_top_elements(T, Is, Rest, Es),
    append(E, Es, Errors).


translate_atom_expression(Expr, Item, Errors) :-
    unpack_atom_expression(Expr, Head, Tail, _),
    ( Tail = [] ->
        translate_atom_head_expression(Head, Item, Errors)
    ;
        translate_atom_head_expression(Head, HeadItem, HeadErrors), 
        Tail = [ArrayIndex|_],
        translate_array_literal(ArrayIndex, TranslatedArrayIndex, IndexErrors),
        Item = array_get(HeadItem, TranslatedArrayIndex),
        append(HeadErrors, IndexErrors, Errors)
    ).
translate_atom_head_expression(Expr, Item, Errors) :-
    unpack_atom_head_expression(Expr, op_un(Op, Head)),
    translate_expression(Head, HeadItem, HeadErrors),
    connect_with_unary_operator(Op, HeadItem, Item, OpErrors),
    append([OpErrors, HeadErrors], Errors).
translate_atom_head_expression(Expr, Item, []) :-
    unpack_atom_head_expression(Expr, int(Item)).
translate_atom_head_expression(Expr, TID, []) :-
    unpack_atom_head_expression(Expr, ident(ID)),
    translate_ident(ID, TID).
translate_atom_head_expression(Expr, call(TID, TRArgs), Errors) :-
    unpack_atom_head_expression(Expr, call_expr(ident(ID), Args)),
    translate_ident(ID, TID),
    translate_expressions(Args, TRArgs, Errors).
translate_atom_head_expression(Expr, array(Items), Errors) :-
    unpack_atom_head_expression(Expr, array(Expressions)),
    translate_expressions(Expressions, Items, Errors).
translate_atom_head_expression(Expr, Item, Errors) :-
    unpack_atom_head_expression(Expr, gen_call(Quantifier, comp_tail(GeneratorExpressions), ConstraintExpression)),
    translate_top_constraint_expression(ConstraintExpression, Constraint, ExpErrors),
    maplist(translate_generator, GeneratorExpressions, Generators, MapErrors),
    Item =.. [constraint, Quantifier, Generators, Constraint],
    append(ExpErrors, MapErrors, StructuredErrors),
    flatten(StructuredErrors, Errors).
translate_atom_head_expression(Expr, Item, Errors) :-
    is_top_expression(Expr),
    translate_expression(Expr, Item, Errors).

translate_generator_vars([], _, [], []).
translate_generator_vars([ident(ID)|IDs], FromName, [in(Name,FromName)|Names], Errors) :-
    translate_ident(ID, Name),
    translate_generator_vars(IDs, FromName, Names, Errors).

translate_generator(gen(IDs, From, GuardExpr), generator(Clauses), Errors) :-
    translate_atom_head_expression(From, FromName, FromErrors),
    translate_generator_vars(IDs, FromName, IDGenerators, IDErrors),
    translate_top_constraint_expression(GuardExpr, GuardConstraint, GuardErrors),
    constraint_to_guard(GuardConstraint, Guard, ReifyErrors),
    append(IDGenerators, Guard, Clauses),
    append([FromErrors, IDErrors, GuardErrors, ReifyErrors], Errors).
    
translate_expressions([], [], []).
translate_expressions([E|Es], [I|Is], Errors) :-
    translate_expression(E, I, Err),
    translate_expressions(Es, Is, Errs),
    append(Err, Errs, Errors).

translate_expressions_list_to_term(Items, Term) :-
    maplist(term_to_atom, Items, AtomicItems),
    atomic_list_concat(AtomicItems, AtomicItem),
    term_to_atom(Term, AtomicItem).

is_top_expression(num_expr(_, _)).
is_top_expression(expr(_, _)).

unpack_top_expression(num_expr(Head, num_expr_binop_tail(empty, empty)), [Head]) :- !.
unpack_top_expression(expr(Head, op_bin_tail(empty, empty)), [Head]) :- !.
unpack_top_expression(num_expr(Head, num_expr_binop_tail(Op, Tail)), [Head,Op|UnpackedTail]) :-
    unpack_top_expression(Tail, UnpackedTail).
unpack_top_expression(expr(Head, op_bin_tail(Op, Tail)), [Head,Op|UnpackedTail]) :-
    unpack_top_expression(Tail, UnpackedTail).

unpack_atom_expression(num_atom(Head, expr_atom_tail(Tail), Anns), Head, Tail, Anns).
unpack_atom_expression(expr_atom(Head, expr_atom_tail(Tail), Anns), Head, Tail, Anns).
unpack_atom_head_expression(num_expr_atom_head(Arg), Arg).
unpack_atom_head_expression(expr_atom_head(Arg), Arg).

translate_operator(Op, Top, Err) :-
    once(translate_operator_(Op, Top, Err)).
translate_operator_(Op, Top, []) :-
    translate_arith_operator(Op, Top).
translate_operator_(Op, Top, []) :-
    translate_set_constructor(Op, Top).
translate_operator_(Op, false, [E]) :- 
    format(atom(E), "- unkown operator ~w", [Op]).

connect_with_operator_(L, Op, R, Item, []) :-
    translate_comp_operator(Op, Top), !,
    Left =.. [is, TL, L],
    Right =.. [is, TR, R],
    Comp =.. [Top, TL, TR],
    Item = (Left, Right, Comp). 
connect_with_operator_(L, Op, R, Item, []) :-
    translate_logical_operator(Op, L, R, Item), !,
    Item = (L ; R).
connect_with_operator_(L, Op, R, Item, []) :-
    translate_set_constructor(Op, Top), !,
    Item =.. [Top, L, R].
connect_with_operator_(L, OP, R, false, [E]) :- 
    format(atom(E), "- unkown operator ~w in ~w ~w ~w", [OP, L, OP, R]).

connect_with_unary_operator(Op, E, Item, []) :-
    translate_unary_operator(Op, Top),
    !,
    Item =.. [Top, E].
connect_with_operator(Op, E, false, [E]) :- 
    format(atom(E), "- unkown operator ~w in ~w ~w", [Op, Op, E]).

and_operator('/\\').

translate_context(_, OP, reified) :-
    reifying_operator(OP), !.
translate_context(C, _, C).

is_expression_operator(X) :-
    translate_arith_operator(X,_).
is_expression_operator(X) :-
    translate_set_constructor(X,_).

is_constraint_operator(X) :-
    translate_comp_operator(X,_).
is_constraint_composer(X) :-
    translate_logical_operator(X,_,_,_).
    

unpack_operator(op_bin_num(OP), UOP) :-
    unpack_operator(OP, UOP), !.
unpack_operator(op_bin_num_builtin(OP), UOP) :-
    unpack_operator(OP, UOP), !.
unpack_operator(op_bin(OP), UOP) :-
    unpack_operator(OP, UOP), !.
unpack_operator(op_bin_builtin(OP), UOP) :-
    unpack_operator(OP, UOP), !.
unpack_operator(op_un_num(OP), UOP) :-
    unpack_operator(OP, UOP), !.
unpack_operator(op_un(OP), UOP) :-
    unpack_operator(OP, UOP), !.
unpack_operator(OP, OP).

translate_arith_operator(OP, TOP) :-    
    unpack_operator(OP, UOP),
    translate_arith_operator_(UOP, TOP).

translate_arith_operator_('+', '+').  
translate_arith_operator_('-', '-').  
translate_arith_operator_('*', '*').  
translate_arith_operator_('/', '//'). 
translate_arith_operator_('div', 'div').

translate_comp_operator(OP, TOP) :-    
    unpack_operator(OP, UOP),
    translate_comp_operator_(UOP, TOP).
translate_comp_operator_('<=', lte). 
translate_comp_operator_('<', lt).
translate_comp_operator_('>=', gte). 
translate_comp_operator_('>', gt).
translate_comp_operator_('==', eq).
translate_comp_operator_('=', eq).
translate_comp_operator_('!=', neq). 

translate_logical_operator(OP, L, R, TOP) :-    
    unpack_operator(OP, UOP),
    translate_logical_operator_(UOP, L, R, TOP).
translate_logical_operator_('\\/', L, R, (L; R)).
translate_logical_operator_('/\\', L, R, (L, R)).
translate_logical_operator_('xor', L, R, ((L ; R), not((L, R)))).
translate_logical_operator_('->', L, R, ((L -> R; true))).
translate_logical_operator_('<-', L, R, ((R -> L; true))).
translate_logical_operator_('<->', L, R, ((L -> R; true), (R -> L; true))).

translate_set_constructor(OP, TOP) :-
    unpack_operator(OP, UOP),
    translate_set_constructor_(UOP, TOP).
translate_set_constructor_('..', 'to').

translate_unary_operator(OP, TOP) :-
    unpack_operator(OP, UOP),
    translate_unary_operator_(UOP, TOP).
translate_unary_operator_('not', 'not').
translate_unary_operator_('+', '+').
translate_unary_operator_('-', '-').

reifying_operator('not').
reifying_operator('xor').
reifying_operator('\\/').
reifying_operator('->').
reifying_operator('<-').
reifying_operator('<->').

ending_operator('/\\').

is_constraint(alldifferent).
is_constraint(circuit).
is_constraint(gte).
is_constraint(gt).
is_constraint(lte).
is_constraint(lt).
is_constraint(eq).
is_constraint(neq).

constraint_to_guard(C, R, []) :-
    C =.. [constraint, OP, A1, A2],
    Guard =.. [guard, OP, V1, V2],
    R = [V1 is A1, V2 is A2, Guard], !.
constraint_to_guard(C, fail, E) :-
    format(atom(E), '- could not reify constraints ~w', [C]).

merge_symbol_tables(ST1, ST2, MST, Errors) :- 
    copy_term(ST1, CST1), 
    copy_term(ST2, CST2),
    assoc_to_list(CST2, PairsToAdd),
    update_symbols(CST1, PairsToAdd, MST, Errors), !.

update_symbols(ST, [], ST, []).
update_symbols(ST, [Symb - SymbValue | RestPairs], MST, Errors) :-
    get_assoc(Symb, ST, SymbValue),
    update_symbols(ST, RestPairs, MST, Errors).
update_symbols(ST, [Symb - SymbValue | RestPairs], MST, [E|Errors]) :- 
    get_assoc(Symb, ST, OldValue),
    OldValue \= SymbValue,
    format(atom(E), "- conflicting references of symbol ~s: ~s / ~s", [Symb, OldValue, SymbValue]),
    update_symbols(ST, RestPairs, MST, Errors).
update_symbols(ST, [Symb - SymbValue | RestPairs], MST, Errors) :-
    \+ get_assoc(Symb, ST, _),
    put_assoc(Symb, ST, SymbValue, TST),
    update_symbols(TST, RestPairs, MST, Errors).
    
:- begin_tests(symbol_table_unit).

st_should_be_equal(ST1, ST2) :- 
    assoc_to_list(ST1, LST1),
    assoc_to_list(ST2, LST2),
    assertion(LST1 == LST2).

test(merge_empty_symbols_tables) :-
    empty_assoc(ST1),
    empty_assoc(ST2),
    merge_symbol_tables(ST1, ST2, MST, []),
    st_should_be_equal(ST1, MST).

test(merge_empty_symbols_table) :-
    empty_assoc(ST1),
    list_to_assoc([s1 - v1, s2 - v2], ST2),
    merge_symbol_tables(ST1, ST2, MST, []),
    st_should_be_equal(ST2, MST).

test(merge_with_empty_symbols_table) :-
    list_to_assoc([s1 - v1, s2 - v2], ST1),
    empty_assoc(ST2),
    merge_symbol_tables(ST1, ST2, MST, []),
    st_should_be_equal(ST1, MST).

test(merge_add_new_symbols) :-
    L1 = [s1 - v1, s2 - v2],
    L2 = [s3 - v3, s4 - v4],
    merge(L1, L2, M),
    list_to_assoc(L1, ST1),
    list_to_assoc(L2, ST2),
    list_to_assoc(M, EST),
    merge_symbol_tables(ST1, ST2, MST, []),
    st_should_be_equal(EST, MST).

test(merge_no_new_symbols) :-
    L1 = [s1 - v1, s2 - v2, s3 - v3],
    L2 = [s1 - v1],
    list_to_assoc(L1, ST1),
    list_to_assoc(L2, ST2),
    merge_symbol_tables(ST1, ST2, MST, []),
    st_should_be_equal(ST1, MST).

test(merge_conflicting_symbol_tables) :-
    L1 = [s1 - v1, s2 - v2, s3 - v3],
    L2 = [s1 - v2, s4 - v4],
    ML = [s1 - v1, s2 - v2, s3 - v3, s4 - v4],
    list_to_assoc(L1, ST1),
    list_to_assoc(L2, ST2),
    list_to_assoc(ML, EST),
    merge_symbol_tables(ST1, ST2, MST, Errs),
    length(Errs, ErrsNum),
    assertion(ErrsNum = 1),
    st_should_be_equal(EST, MST).

test(merge_updating_symbol_tables) :-
    L1 = [s1 - v1, s2 - v(_), s3 - v3],
    L2 = [s4 - v4, s2 - v(t2)],
    ML = [s1 - v1, s2 - v(t2), s3 - v3, s4 - v4],
    list_to_assoc(L1, ST1),
    list_to_assoc(L2, ST2),
    list_to_assoc(ML, EST),
    merge_symbol_tables(ST1, ST2, MST, []),
    st_should_be_equal(EST, MST),
    assertion(L1 = [s1 - v1, s2 - v(ot), s3 - v3]).

:- end_tests(symbol_table_unit).