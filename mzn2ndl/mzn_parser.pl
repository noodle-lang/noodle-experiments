:- module(mzn_parser, [
    parse_mzn_file/2
]).

:- use_module(library(dcg/basics)).
:- use_module(library(dcg/high_order)).

parse_mzn_file(Path, Model) :- 
    phrase_from_file(without_comments(Code), Path),
    phrase(model(Model), Code).
    
% Comment parsing 
without_comments([]) --> eos.
without_comments(OUT) --> string_token(STR), without_comments(REST), { string_concat(STR, REST, OUT)}, !.
without_comments(OUT) --> (comment_multiline ; comment_singleline; "\n"), without_comments(OUT), !.
without_comments(OUT) --> [H], without_comments(Rest), { OUT = [H | Rest] }.

comment_singleline --> "%", string_without("\n", _), ("\n" ; eos).
comment_multiline --> "/*", string(_), "*/", !. 
string_token(OUT) --> quote_token(B), string_contents(C), quote_token(E), { string_codes(C, CC), append([B,CC,E], OUT) }.
quote_token('"') --> "\"".

% A MiniZinc model

model(model(Items)) --> blanks, item(H), !, p_model_tail(Rest), { Items = [H | Rest] }.
p_model_tail([]) --> optional(h_semicolon, []), eos, !.
p_model_tail(Items) --> h_semicolon, item(H), !, p_model_tail(Rest), { Items = [H | Rest] }. 
% Items
item(item(I)) --> (
                  constraint_item(I);  
                  include_item(I);
                  var_decl_item(I);
                  enum_item(I);
                  assign_item(I);
                  solve_item(I);
                  output_item(I);
                  predicate_item(I);
                  test_item(I);
                  function_item(I);
                  annotation_item(I)
                )
                  , !.
item(item(I)) --> error_item(I).
                  

error_item(item_error(Error)) --> string_without(";", ErrorCodes), { string_codes(Error, ErrorCodes) }.
% Include items
include_item(item_include(X)) --> "include", h_space, string_literal(X).

% Variable declaration items
var_decl_item(item_var_decl(Id, Expr, Anns)) --> ti_expr_and_id(Id), annotations_or_blanks(Anns), h_eq, expr(Expr), !.
var_decl_item(item_var_decl(Id, empty, Anns)) --> ti_expr_and_id(Id), annotations_or_blanks(Anns).                                          

% Enum items
enum_item(item_enum(Name, Cases, Anns)) --> "enum", h_space, ident(Name), Anns, 
                                            optional((h_eq, enum_cases(Cases)), { Cases = empty}). 
enum_cases(enum_cases(Cases)) --> sequence(h_l_curly_bracket, ident, h_comma, h_r_curly_bracket, Cases).


% Assign items
assign_item(item_assign(Name, Expr)) --> ident(Name), h_eq, expr(Expr).

% Constraint items
constraint_item(item_constraint(Expr, Ann)) --> "constraint",
                                                string_annotation_or_space(Ann),
                                                expr(Expr).

% Solve item
solve_item(item_solve(sat, empty, Anns)) --> "solve", annotations_or_space(Anns), "satisfy".
solve_item(item_solve(min, Expr, Anns)) --> "solve", annotations_or_space(Anns), "minimize", h_space, expr(Expr).
solve_item(item_solve(max, Expr, Anns)) --> "solve", annotations_or_space(Anns), "maximize", h_space, expr(Expr).

% Output items
output_item(item_output(Expr)) --> "output", h_space, expr(Expr).

% Annotation items
annotation_item(item_annotation(Name, Params)) --> "annotation", h_space, ident(Name), blanks, params(Params).

% Predicate, test and function items
predicate_item(item_predicate(Op)) --> "predicate", h_space, operation_item_tail(Op).

test_item(item_test(Op)) --> "test", h_space, operation_item_tail(Op).

function_item(item_function(Type, Op)) --> "function", h_space, ti_expr(Type), h_colon, operation_item_tail(Op).

operation_item_tail(item_operation_tail(Name, Params, Body, Anns)) --> ident(Name), blanks, params(Params), annotations_or_blanks(Anns),
                                                                       optional((h_eq, expr(Body)), { Body = empty }).

params(params(Params)) --> sequence(h_l_round_bracket, ti_expr_and_id, h_comma, h_r_round_bracket, Params).


ti_expr_and_id(element(ID, Type)) --> ti_expr(Type), h_colon, ident(ID).

ti_expr(E) --> base_ti_expr(E).
base_ti_expr(type(Mode, Name)) --> var_par(Mode), blanks, base_ti_expr_tail(Name). 

var_par(var) --> "var ".
var_par(par) --> "par "; "".

base_type(T) --> string(X), { p_base_type(X), atom_codes(T, X) }.

p_base_type(`bool`). 
p_base_type(`int`).
p_base_type(`float`). 
p_base_type(`string`).

base_ti_expr_tail(BaseType) --> base_type(BaseType).
base_ti_expr_tail(Set) --> set_ti_expr_tail(Set).
base_ti_expr_tail(Array) --> array_ti_expr_tail(Array). 
base_ti_expr_tail(ID) --> ident(ID).
base_ti_expr_tail(ann_type) --> "ann".
base_ti_expr_tail(opt_type(Type)) --> "opt", h_space, base_ti_expr_tail(Type).
base_ti_expr_tail(set_explicit(Expressions)) --> sequence(h_l_curly_bracket, expr, h_comma, h_r_curly_bracket, Expressions).
base_ti_expr_tail(set_call(Call)) --> call_expr(Call).
base_ti_expr_tail(range(Start, End)) --> num_expr(Start), h_double_dot, num_expr(End).

% TODO: Specification allows only basetypes in sets, examples however use freely the generic expressions
% Solve this issue in a general manner.
set_ti_expr_tail(set_type(Type)) --> "set", h_space, "of", h_space, base_ti_expr_tail(Type).                          
                                 
array_ti_expr_tail(array_type(Index, Type)) --> "array", 
                        blanks, 
                        p_array_ti_expr_tail_index(Index),
                        blanks,
                        "of",
                        h_space,
                        ti_expr(Type).
array_ti_expr_tail(list_type(Type)) -->  "list",
                        h_space, 
                        "of",
                        h_space,
                        ti_expr(Type).
p_array_ti_expr_tail_index(IndexTypes) --> sequence(h_l_square_bracket, ti_expr, h_comma, h_r_square_bracket, IndexTypes).


% Expressions
expr(expr(Head, Tail)) --> expr_atom(Head), expr_binop_tail(Tail).

expr_atom(expr_atom(Head, Tail, Anns)) --> expr_atom_head(Head), expr_atom_tail(Tail), annotations_or_blanks(Anns).


expr_binop_tail(op_bin_tail(Op, Expr)) --> bin_op(Op), !, expr(Expr).
expr_binop_tail(op_bin_tail(empty, empty)) --> [].

expr_atom_head(expr_atom_head(Head)) --> 
                                         builtin_un_op(Op), blanks, expr_atom(Expr), { Head = op_un(Op, Expr) };
                                         h_l_round_bracket, expr(Head), h_r_round_bracket;
                                         "_",  { Head = anon_var };
                                         bool_literal(Head);
                                         int_literal(Head);
                                         float_literal(Head);
                                         string_literal(Head);
                                         set_literal(Head);
                                         set_comp(Head);
                                         array_literal(Head);
                                         array_literal_2d(Head);
                                         array_comp(Head);
                                        %  ann_literal(Head); TODO: ambigous with call_expr
                                         if_then_else_expr(Head);
                                         let_expr(Head);
                                         gen_call_expr(Head);
                                         call_expr(Head);
                                         ident_or_quoted_op(Head).

expr_atom_tail(expr_atom_tail(Tail)) --> sequence(array_literal, Tail).

% Numeric expressions
num_expr(num_expr(A, T)) --> num_expr_atom(A), num_expr_binop_tail(T).
num_expr_atom(num_atom(Head, Tail, Anns)) --> num_expr_atom_head(Head), expr_atom_tail(Tail), annotations_or_blanks(Anns).

num_expr_binop_tail(num_expr_binop_tail(Op, Expr)) --> num_bin_op(Op), !, num_expr(Expr).
num_expr_binop_tail(num_expr_binop_tail(empty, empty)) --> [].

num_expr_atom_head(num_expr_atom_head(Head)) --> builtin_num_un_op(OP), num_expr_atom(Expr), { Head = op_un(OP, Expr) };
                                                 h_l_round_bracket, num_expr(Head), h_r_round_bracket;
                                                 int_literal(Head);
                                                 float_literal(Head);
                                                 if_then_else_expr(Head);
                                                 let_expr(Head);
                                                 gen_call_expr(Head);
                                                 call_expr(Head);
                                                 ident_or_quoted_op(Head).

% Built-in operators
builtin_op(op_builtin(OP)) --> builtin_bin_op(OP) ; builtin_un_op(OP).

bin_op(op_bin(OP)) --> builtin_bin_op(OP) ; (h_apostrophe, ident(OP), h_apostrophe).

builtin_bin_op(op_bin_builtin(OP)) --> {p_builtin_bin_op(X), atom_codes(OP, X) }, blanks, string(X), blanks.
builtin_bin_op(op_bin_builtin(OP)) -->  builtin_num_bin_op(OP).

p_builtin_bin_op(`<->`). p_builtin_bin_op(`<-`). p_builtin_bin_op(`<=`). p_builtin_bin_op(`<`).
p_builtin_bin_op(`>=`). p_builtin_bin_op(`>`). 
p_builtin_bin_op(`==`). p_builtin_bin_op(`=`). 
p_builtin_bin_op(`++`). p_builtin_bin_op(`->`).  
p_builtin_bin_op(`\\/`). p_builtin_bin_op(`xor`). p_builtin_bin_op(`/\\`).  

p_builtin_bin_op(`!=`). p_builtin_bin_op(`in`). p_builtin_bin_op(`subset`). p_builtin_bin_op(`superset`). 
p_builtin_bin_op(`union`). p_builtin_bin_op(`diff`). p_builtin_bin_op(`symdiff`). p_builtin_bin_op(`..`). 
p_builtin_bin_op(`intersect`).  

builtin_un_op(OP) --> builtin_num_un_op(OP).
builtin_un_op(op_un(not)) --> "not". 

% Built-in numeric operators
num_bin_op(op_bin_num(OP)) --> builtin_num_bin_op(OP) ; (h_apostrophe, ident(OP), h_apostrophe).

builtin_num_bin_op(op_bin_num_builtin(OP)) --> blanks, string(X), blanks, { p_num_bin_op(X), atom_codes(OP, X) }.
p_num_bin_op(`+`). p_num_bin_op(`-`). p_num_bin_op(`*`). 
p_num_bin_op(`/`). p_num_bin_op(`div`). p_num_bin_op(`mod`).

builtin_num_un_op(op_un_num(OP)) --> blanks, string(X), blanks, { p_num_un_op(X), atom_codes(OP, X) }.
p_num_un_op(`+`).
p_num_un_op(`-`).

% Boolean literals
bool_literal(bool(false)) --> "false".
bool_literal(bool(true)) --> "true".

% Integer literals
% TODO: missing octal system
int_literal(int(Int)) --> integer(Int).
int_literal(int(Int)) --> "0x", xinteger(Int).

% Float literals
% TODO: missing different systems (oct, hex, etc)
float_literal(float(Float)) --> float(Float).

% String literals
string_contents(S) --> [H],  { \+ p_string_non_char(H) }, !, string_contents(T), { string_codes(HS, [H]), string_concat(HS, T, S) }.
string_contents(S) --> "\\", [H], { \+ p_string_non_escape(H) }, !, string_contents(T), { string_codes(HS, [H]), string_concat(HS, T, S) }.
string_contents("") --> "".

p_string_non_char(10). p_string_non_char(34). p_string_non_char(92).
p_string_non_escape(10). p_string_non_escape(40).

string_literal(string_literal(S, empty)) --> h_quote, string_contents(S), h_quote.
string_literal(string_literal(S, T)) --> h_quote, string_contents(S), h_escape, h_l_round_bracket, string_interpolate_tail(T).

string_interpolate_tail(string_interpolate(E, string_literal(S, empty))) --> expr(E), h_r_round_bracket, string_contents(S), h_quote, !.
string_interpolate_tail(string_interpolate(E, string_literal(S, T))) --> expr(E), h_r_round_bracket, string_contents(S), h_escape, h_l_round_bracket, string_interpolate_tail(T).

% Set literals
set_literal(set(Elements)) --> sequence(h_l_curly_bracket, expr, h_comma, h_r_curly_bracket, Elements). 

% Set comprehensions
set_comp(comp_set(E,C)) --> h_l_curly_bracket, expr(E), h_pipe, comp_tail(C), h_r_curly_bracket.

comp_tail(comp_tail(Cs)) --> sequence(generator, h_comma, Cs).

generator(gen(IDs, From, Where)) --> sequence(ident, h_comma, IDs), h_space, "in", h_space, expr(From), optional(p_guard(Where), { Where = expr(bool(true)) }).
p_guard(G) --> blanks, "where", blanks, expr(G).


% Array literals
array_literal(array(ELs)) --> h_l_square_bracket, p_array_literal_contents(ELs), h_r_square_bracket.
p_array_literal_contents(ELs) --> sequence(expr, h_comma, ELs).

% 2D Array literals
array_literal_2d(array2d([E|Tail])) --> h_l_square_pipe, p_array_literal_contents(E), p_array_literal_2d_tail(Tail).
p_array_literal_2d_tail([]) --> h_r_square_pipe, !.
p_array_literal_2d_tail([E|Tail]) --> h_pipe, p_array_literal_contents(E), p_array_literal_2d_tail(Tail).

% Array comprehensions
array_comp(comp_array(E,C)) --> h_l_square_bracket, expr(E), h_pipe, comp_tail(C), h_r_square_bracket.

% Array access
array_access_tail(array_index(AllELs)) --> h_l_square_bracket, expr(E), p_array_literal_contents(ELs), h_r_square_bracket,
                                           { AllELs = [E | ELs] }.

% Annotation literals
ann_literal(ann_lit(ID, Body)) --> ident(ID), sequence(h_l_round_bracket, expr, h_comma, h_r_round_bracket, Body).

% If-then-else expressions
if_then_else_expr(if(Branches, Else)) --> 
    "if", h_space, expr(If), "then", h_space, expr(Then),
    sequence(p_if_then_else_expr_elseif, ElseConds),
    "else", h_space, expr(Else), "endif", 
    { Branches = [cond(If, Then) | ElseConds] }.

p_if_then_else_expr_elseif(cond(If, Then)) -->
    "elseif", h_space, expr(If), "then", h_space, expr(Then).

% Call expressions
call_expr(call_expr(ID, Args)) --> ident_or_quoted_op(ID), blanks, h_l_round_bracket, !, p_call_expr_tail(Args).
% TODO: No support for predicates/functions withouth arguments
% call_expr(call_expr(ID, [])) --> ident_or_quoted_op(ID).

p_call_expr_tail([H]) --> expr(H), h_r_round_bracket.  
p_call_expr_tail([H|T]) --> expr(H), h_comma, p_call_expr_tail(T), !.
% Let expressions
let_expr(let(Defs, Expr)) --> "let", blanks, h_l_curly_bracket, 
                         sequence(let_item, h_semicolon, Defs), h_r_curly_bracket,
                         h_space, "in", h_space, expr(Expr).

let_item(I) --> var_decl_item(I) ; constraint_item(I).

% Generator call expressions
gen_call_expr(gen_call(Q, C, E)) --> ident_or_quoted_op(Q), blanks, h_l_round_bracket, 
                                     comp_tail(C), h_r_round_bracket, blanks,
                                     h_l_round_bracket, expr(E), h_r_round_bracket.

% Miscellaneous Elements
ident(ident(ID)) --> 
    p_ident_first_char(F), 
    sequence(p_ident_rest_char, R), !,
    { format(string(ID), "~s", [[F|R]]), \+ h_keyword(ID) }.
p_ident_first_char(C) --> [C], { code_type(C, ascii), code_type(C, alpha) }.
p_ident_rest_char(C) --> [C], { code_type(C, ascii), code_type(C, csym) }.

ident_or_quoted_op(ID) --> ident(ID).
ident_or_quoted_op(OP) --> h_apostrophe, builtin_op(OP), h_apostrophe.

% Annotations
annotations(anns(Anns)) --> annotation(A), sequence(annotation, As), blanks, { Anns = [A | As]}.

annotations_or_space(Anns) --> annotations(Anns), !.
annotations_or_space(anns([])) --> h_space.

annotations_or_blanks(Anns) --> annotations(Anns), !.
annotations_or_blanks(anns([])) --> blanks.

annotation(ann(Head, Tail)) --> h_double_colon, expr_atom_head(Head), expr_atom_tail(Tail).

string_annotation_or_space(Ann) --> string_annotation(Ann), !.
string_annotation_or_space(empty) --> h_space.
string_annotation(ann_str(Lit)) --> h_double_colon, string_literal(Lit).

% Helpers

h_keyword("ann"). h_keyword("annotation"). h_keyword("any"). h_keyword("array"). 
h_keyword("bool"). h_keyword("case"). h_keyword("constraint"). h_keyword("diff"). 
h_keyword("div"). h_keyword("else"). h_keyword("elseif"). h_keyword("endif"). 
h_keyword("enum"). h_keyword("false"). h_keyword("float"). h_keyword("function"). 
h_keyword("if"). h_keyword("in"). h_keyword("include"). h_keyword("int"). 
h_keyword("intersect"). h_keyword("let"). h_keyword("list"). h_keyword("maximize"). 
h_keyword("minimize"). h_keyword("mod"). h_keyword("not"). h_keyword("of"). 
h_keyword("op"). h_keyword("opt"). h_keyword("output"). h_keyword("par"). 
h_keyword("predicate"). h_keyword("record"). h_keyword("satisfy"). h_keyword("set"). 
h_keyword("solve"). h_keyword("string"). h_keyword("subset"). h_keyword("superset"). 
h_keyword("symdiff"). h_keyword("test"). h_keyword("then"). h_keyword("true"). 
h_keyword("tuple"). h_keyword("type"). h_keyword("union"). h_keyword("var"). 
h_keyword("where"). h_keyword("xor").

h_l_square_bracket --> "[", blanks.
h_r_square_bracket --> blanks, "]".
h_l_curly_bracket --> "{", blanks.
h_r_curly_bracket --> blanks, "}".
h_l_round_bracket --> "(", blanks.
h_r_round_bracket --> blanks, ")".
h_l_square_pipe --> "[", h_pipe.
h_r_square_pipe --> h_pipe, "]".
h_comma --> blanks, ",", blanks.
h_double_dot --> blanks, "..", blanks.
h_colon --> blanks, ":", blanks.
h_semicolon --> blanks, ";", blanks.
h_space --> " ", blanks.
h_double_colon --> blanks, "::", blanks.
h_apostrophe --> blanks, "'", blanks.
h_quote --> blanks, "\"", blanks.
h_pipe --> blanks, "|", blanks.
h_escape --> "\\".
h_eq --> blanks, "=", blanks.