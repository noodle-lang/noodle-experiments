#!/usr/bin/env swipl

:- use_module(library(noodle)).
:- use_module(mzn_parser).
:- use_module(mzn_model).
:- use_module(library(assoc)).
:- use_module(library(yall)).
:- initialization(main,main).

main([MznPath]) :-
%    debug(mzn_model),
    parse_mzn_file(MznPath, MznAst),
    create_mzn_model(MznAst, MznModel, Errors),
    print_errors(Errors),
    print_model(MznModel).

print_errors([]).
print_errors([E|Es]) :-
    debug(mzn_model, '~w', [E]),
    print_errors(Es).

print_model(Model) :-
    print_header(),
    print_parameters(Model),
    print_model_builder(Model).

print_header() :-
    writeln("#!/usr/bin/env swipl"),
    writeln("% Header"),
    writeln(":- use_module(library(arithmetic))."),
    writeln(":- use_module(library(noodle))."),
    nl.

print_model_builder(Model) :-
    writeln("% Builder"),
    Head = build(Path),
    model_builder(Model, Parameters, Builder),
    write_term((Head :- Parameters,  ndl_model(_{serialize:Path}, Builder)), []), 
    writeln(".").

model_builder(Model, Parameters, Builder) :-   
    assoc_to_keys(Model.symbols, Symbols), 
    include(is_variable, Symbols, Variables),
    define_variables(Variables, Types, Parameters, VarBuilder),
    debug(mzn_model, '~w', [Model.constraints]),
    define_constraints(Model.constraints, Types, ConstraintBuilder),
    debug(mzn_model, '~w', [ConstraintBuilder]),
    Builder = (VarBuilder, ConstraintBuilder).

% #region parameters  
print_parameters(Model) :-
    writeln("% Parameters"),
    assoc_to_keys(Model.symbols, Symbols),
    print_int_parameters(Model.symbols, Symbols),
    print_set_parameters(Model.symbols, Symbols),
    print_array_parameters(Model.symbols, Symbols),
    nl.

print_int_parameters(Model, Symbols) :-
    include(is_int_parameter, Symbols, IntParameters),
    forall(member(P, IntParameters), (
       write_term(parameter_int(P.id), []), 
       write(".\n") 
    )),   
    forall(member(P, IntParameters), (
        get_assoc(P, Model, V),
        print_int_parameter(P,V)        
    )).
print_set_parameters(Model, Symbols) :-
    include(is_set_parameter, Symbols, SetParameters),
    forall(member(P, SetParameters), (
            write_term(parameter_set(P.id), []), 
            write(".\n") 
    )),
    forall(member(P, SetParameters), (
        get_assoc(P, Model, V),
        print_set_parameter(P,V)
    )),
    forall(member(P, SetParameters), (
        print_in_set_parameter(P)
    )).
print_array_parameters(Model, Symbols) :-
    include(is_array_parameter, Symbols, ArrayParameters),
    forall(member(P, ArrayParameters), (
            P.type = array_type(IndexTypes, _),
            length(IndexTypes, Dimensions),
            write_term(parameter_array(P.id, Dimensions), []), 
            write(".\n") 
    )),
    forall(member(P, ArrayParameters), (
        get_assoc(P, Model, V),
        print_array_parameter(P,V)        
    )).
% #endregion parameters

% #region variables
define_variables(Variables, Types, ParameterDefinitions, VariableDefinitions) :-
    empty_assoc(DefinedSymbols),
    define_variables(Variables, DefinedSymbols, Types, ParameterDefinitionsList, VariableDefinitionsList),
    list_to_conjuct(VariableDefinitionsList, VariableDefinitions),
    list_to_conjuct(ParameterDefinitionsList, ParameterDefinitions).

define_variables([], _, [], [], []).
define_variables([Var|Rest], DefinedSymbols, Types, ParDefinitions, VarDefinitions) :-
    variable_definition(Var, DefinedSymbols, NewParDefinitions, NewVarDefinitions, NewDefinedSymbols, NewTypes),
    define_variables(Rest, NewDefinedSymbols, RestTypes, RestParDefinitions, RestVarDefinitions),
    append(NewVarDefinitions, RestVarDefinitions, VarDefinitions),
    append(NewParDefinitions, RestParDefinitions, ParDefinitions),
    append(NewTypes, RestTypes, Types).

variable_definition(Var, DefinedSymbols, ParDefinitions, VarDefinitions, NewDefinedSymbols, [var(Var.id) | RangeTypes]) :- 
    Var.type = int, !,
    define_range_if_undefined(single_element, DefinedSymbols, 1..1, ParIndexDefinitions, VarIndexDefinitions, IndexDefinedSymbols, RangeTypes1),
    define_range_if_undefined(int_range, IndexDefinedSymbols, -2147483648..2147483647, ParDomainDefinitions, VarDomainDefinitions, NewDefinedSymbols, RangeTypes2),
    VarNewDefinitions = [ define_variable(Var.id, [range(single_element)], range(int)) ],
    append([ParIndexDefinitions, ParDomainDefinitions], ParDefinitions),
    append([VarIndexDefinitions, VarDomainDefinitions, VarNewDefinitions], VarDefinitions),
    append(RangeTypes1, RangeTypes2, RangeTypes).
variable_definition(Var, DefinedSymbols, ParDefinitions, VarDefinitions, NewDefinedSymbols, [var(Var.id) | RangeTypes]) :-
    Var.type = set_type(FromExpr to ToExpr), !,
    define_range_if_undefined(single_element, DefinedSymbols, 1..1, ParIndexDefinitions, VarIndexDefinitions, NewDefinedSymbols, RangeTypes),
    VarNewDefinitions = [ define_variable(Var.id, [range(single_element)], From..To) ],
    append(ParIndexDefinitions, [From is FromExpr, To is ToExpr], ParDefinitions),
    append(VarIndexDefinitions, VarNewDefinitions, VarDefinitions).
variable_definition(Var, DefinedSymbols, ParDefinitions, VarDefinitions, NewDefinedSymbols, NewTypes) :-
    Var.type = array_type(Indexes, Type), !,
    define_array_indexes(Indexes, DefinedSymbols, ParIndexDefinitions, VarIndexDefinitions, IndexDefinedSymbols, ArrayTypes),
    define_range_if_undefined(Type, IndexDefinedSymbols, ParDomDefinitions, VarDomDefinitions, NewDefinedSymbols, RangeTypes),
    array_var_definition(Var.id, Indexes, Type, ParArrayDefinitions, VarArrayDefinitions, VarTypes),
    append([ParIndexDefinitions, ParDomDefinitions, ParArrayDefinitions], ParDefinitions),
    append([VarIndexDefinitions, VarDomDefinitions, VarArrayDefinitions], VarDefinitions),
    append([ArrayTypes, RangeTypes, VarTypes], NewTypes).

array_var_definition(Name, Indexes, Type, ParDefinitions, [ArrayDefine], [var(Name)]) :-
    build_array_var_dom_definition(Type, ParDomDefinitions, DomTerm),
    build_array_var_index_definition(Indexes, ParIndexDefinitions, IndexTerms),
    append(ParIndexDefinitions, ParDomDefinitions, ParDefinitions),
    ArrayDefine =.. [define_variable, Name, IndexTerms, DomTerm].
    
build_array_var_dom_definition(range(FromExpr,ToExpr), [From is FromExpr, To is ToExpr], From .. To) :- !.
build_array_var_dom_definition(RangeName, [], range(RangeName)).

build_array_var_index_definition([], [], []).
build_array_var_index_definition([range(FromExpr, ToExpr)|Indexes], [F,T|Bs], [E|Es]) :- !,
    F =.. [is, From, FromExpr], 
    T =.. [is, To, ToExpr],
    E = to(From,To),  
    build_array_var_index_definition(Indexes, Bs, Es).
build_array_var_index_definition([RangeName|Indexes], Bs, [range(RangeName)|Es]) :-
    build_array_var_index_definition(Indexes, Bs, Es).

define_array_indexes([], DefinedSymbols, [], [], DefinedSymbols, []).
define_array_indexes([set_type(_ to _)|Rest], DefinedSymbols, ParDefinitions, VarDefinitions, NewDefinedSymbols, RestTypes) :-
    !, define_array_indexes(Rest, DefinedSymbols, ParDefinitions, VarDefinitions, NewDefinedSymbols, RestTypes).
define_array_indexes([RangeName|Rest], DefinedSymbols, ParDefinitions, VarDefinitions, NewDefinedSymbols, NewTypes) :-
    define_range_if_undefined(RangeName, DefinedSymbols, ParRangeDefinitions, VarRangeDefinitions, RangeSymbols, RangeTypes),
    define_array_indexes(Rest, RangeSymbols, ParRestDefinitions, VarRestDefinitions, NewDefinedSymbols, RestTypes),
    append(ParRangeDefinitions, ParRestDefinitions, ParDefinitions),
    append(VarRangeDefinitions, VarRestDefinitions, VarDefinitions),
    append(RangeTypes, RestTypes, NewTypes).

define_range_if_undefined(range(_,_), DefinedSymbols, [], [], DefinedSymbols, []) :- !.
define_range_if_undefined(Name, DefinedSymbols,ParDefinitions, VarDefinitions, NewDefinedSymbols, NewTypes) :-
    (get_assoc(Name, DefinedSymbols, _) ->
        ParDefinitions = [],
        VarDefinitions = [],
        NewDefinedSymbols = DefinedSymbols,
        NewTypes = []
    ;
        ParDefinition =.. [Name, From .. To],
        ParDefinitions = [ParDefinition],
        VarDefinitions = [ define_range(Name, From .. To) ],
        put_assoc(Name, DefinedSymbols, t, NewDefinedSymbols),
        NewTypes = [range(Name)]
    ).
define_range_if_undefined(Name, Value, DefinedSymbols, [], VarDefinitions, NewDefinedSymbols, NewTypes) :-
    (get_assoc(Name, DefinedSymbols, _) ->
        VarDefinitions = [],
        NewDefinedSymbols = DefinedSymbols,
        NewTypes = []
    ;
        VarDefinitions = [ define_range(Name, Value) ],
        put_assoc(Name, DefinedSymbols, t, NewDefinedSymbols),
        NewTypes = [range(Name)]
    ).

print_int_parameter(P, V) :-
    OutputTermHead =.. [P.id, Value],
    OutputTermBody =.. [is, Value, V],
    OutputTerm = (OutputTermHead :- OutputTermBody),
    format(":- arithmetic_function(~w/0).\n", [P.id]),
    write_term(OutputTerm, []), writeln(".").

print_set_parameter(P, FromExpr to ToExpr) :-
    OutputTermHead =.. [P.id, From..To],
    OutputTermBodyFrom =.. [is, From, FromExpr],
    OutputTermBodyTo =.. [is, To, ToExpr],
    OutputTerm = (OutputTermHead :- OutputTermBodyFrom, OutputTermBodyTo),
    write_term(OutputTerm, []), writeln(".").

print_in_set_parameter(P) :-
    ParamAtom =.. [P.id, From..To],
    IsElementTerm = (in(El, P.id) :- ParamAtom, between(From, To, El)),
    write_term(IsElementTerm, []), writeln(".").

print_array_parameter(P, array(Exprs)) :-
    P.type = array_type(IndexTypes, _),
    length(IndexTypes, NumDimensions),
    length(Variables, NumDimensions),
    append([[P.id], Variables, [Value]], HeadComponents),
    OutputTermHead =.. HeadComponents,
    OutputTerm = (OutputTermHead :- 
        calculate_array_index(IndexTypes, Variables, Index), 
        nth0(Index, Exprs, Expr),
        Value is Expr),
    format(":- arithmetic_function(~w/~w).\n", [P.id, NumDimensions]),
    write_term(OutputTerm, []), writeln(".").

print_array_parameter(P, call(_, Params)) :-
    reverse(Params, [Array | _]),
    print_array_parameter(P, Array).

print_parameter(P, V) :-
    format("~w - ~w.", [P, V]).
        
is_int_parameter(S) :- S.mode = par, S.type = int.
is_set_parameter(S) :- S.mode = par, S.type = set_type(_).
is_array_parameter(S) :- S.mode = par, S.type = array_type(_,_).
is_variable(S) :- S.mode = var.    
% #endregion variables
    
% #region constraints
define_constraints([], _, [], []).
define_constraints([C|Rest], Types, EBuilders, SBuilders) :- 
    constraint_definition(C, Types, EB, SB),
    define_constraints(Rest, Types, EBTail, SBTail),
    append(EB, EBTail, EBuilders),
    append(SB, SBTail, SBuilders).

define_constraints(Constraints, Types, Builders) :-
    define_constraints(Constraints, Types, EBuilders, SBuilders),
    debug(mzn_model, '- got constraints ~w / ~w', [EBuilders, SBuilders]),
    append(EBuilders, SBuilders, BuildersList),
    list_to_conjuct(BuildersList, Builders).

constraint_semantics([], _, [], [], []).
constraint_semantics([C|T], Types, Exprs, [O|TO], Vars) :-
    constraint_semantic(C, Types, E, O, V),
    constraint_semantics(T, Types, TE, TO, TV),
    append(E, TE, Exprs),
    append(V, TV, Vars).

constraint_semantic(Variable, _, [], Variable, []) :- var(Variable), !, debug(mzn_model, '- extension of var ~w', [Variable]).
constraint_semantic(Atom, _, [], Atom, []) :- atom(Atom), !, debug(mzn_model, '- extension of atom ~w', [Atom]).
constraint_semantic(Number, _, [], Number, []) :- number(Number), !, debug(mzn_model, '- extension of number ~w', [Number]).

constraint_semantic(array_get(Name, array(Index)), Types, Exprs, Output, [Output-var(Name)]) :-
    member(var(Name), Types), !,
    % TODO: add indexing with variables
    debug(mzn_model, '- extensions of var get ~w', [array_get(Name, array(Index))]),
    constraint_semantics(Index, Types, IndexExprs, IndexVars, _),
    append([[get_value, Name], IndexVars, [Output]], VarExprList),
    VarExpr =.. VarExprList,
    append(IndexExprs, [VarExpr], Exprs).
constraint_semantic(array_get(Name, array(Index)), Types, Expr, Output, []) :-
    % TODO: add indexing with variables
    \+ member(var(Name), Types),
    !,
    debug(mzn_model, '- extensions of array get ~w', [array_get(Name, array(Index))]),
    constraint_semantics(Index, Types, IndexExprs, IndexVars, _),
    ArrayGetter =.. [Name | IndexVars],
    append(IndexExprs, [Output is ArrayGetter], Expr).
constraint_semantic(Expr, Types, [ArgExpr, O is TrExpr], O, ArgVariable) :-
    Expr =.. [Op, Arg],
    debug(mzn_model, '- extension of unary op ~w', [Expr]),
    constraint_expression(Arg, Types, ArgExpr, ArgOutput, ArgVariable),
    TrExpr =.. [Op, ArgOutput].
constraint_semantic(Expr, Types, ConstrExprs, O, Var) :-
    Expr =.. [Op, Arg1, Arg2],
    debug(mzn_model, '- extension of binary op ~w', [Expr]),
    constraint_semantic(Arg1, Types, Arg1Expr, A1, Var1),
    constraint_semantic(Arg2, Types, Arg2Expr, A2, Var2),
    TrExpr =.. [Op, A1, A2],
    append([Arg1Expr, Arg2Expr, [O is TrExpr]], ConstrExprs),
    append(Var1, Var2, Var).

constraint_extensions([], _, [], [], [], [], []).
constraint_extensions([C|T], Types, Exprs, [O|TO], SemExprs, [SO|TSO], Vars) :-
    constraint_extension(C, Types, E, O, SE, SO, V),
    constraint_extensions(T, Types, TE, TO, TSE, TSO, TV),
    append(E, TE, Exprs),
    append(V, TV, Vars),
    append(SE, TSE, SemExprs).

constraint_extension(Variable, _, [], Variable, [], Variable, []) :- var(Variable), !, debug(mzn_model, '- extension of var ~w', [Variable]).
constraint_extension(Atom, _, [], Atom, [], Atom, []) :- atom(Atom), !, debug(mzn_model, '- extension of atom ~w', [Atom]).
constraint_extension(Number, _, [], Number, [], Number, []) :- number(Number), !, debug(mzn_model, '- extension of number ~w', [Number]).

constraint_extension(array_get(Name, array(Index)), Types, Exprs, Output, [SemExpr], SemOutput, [Output-var(Name)-Input]) :-
    member(var(Name), Types), !,
    % TODO: add indexing with variables
    debug(mzn_model, '- extensions of var get ~w', [array_get(Name, array(Index))]),
    constraint_extensions(Index, Types, IndexExprs, IndexVars, _, _, _),
    append([[variable, Name], IndexVars, [Output]], VarExprList),
    VarExpr =.. VarExprList,
    append(IndexExprs, [VarExpr], Exprs), 
    SemExpr =.. [get_value, Input, SemOutput].
constraint_extension(array_get(Name, array(Index)), Types, Expr, Output, Expr, Output, []) :-
    % TODO: add indexing with variables
    !,
    debug(mzn_model, '- extensions of array get ~w', [array_get(Name, array(Index))]),
    constraint_extensions(Index, Types, IndexExprs, IndexVars, _, _, _),
    append([[Name], IndexVars, [Output]], ArrayGetterList),
    ArrayGetter =.. ArrayGetterList,
    append(IndexExprs, [ArrayGetter], Expr).
constraint_extension(Expr, Types, TrExprs, O, SemExprs, SemO, ArgVariable) :-
    Expr =.. [Op, Arg],
    debug(mzn_model, '- extension of unary op ~w', [Expr]),
    constraint_expression(Arg, Types, ArgExpr, ArgOutput, ArgSemExpr, ArgSemOutput, ArgVariable),
    SemExpr =.. [Op, ArgSemOutput],
    SemExprs = [ArgSemExpr, SemO is SemExpr],
    (
        ArgVariable = [] -> (
            TrExpr =.. [Op, ArgOutput],
            TrExprs = [ArgExpr, O is TrExpr]
        );(
            O = ArgOutput,
            TrExprs = ArgExpr
        )
    ).
constraint_extension(Expr, Types, ConstrExprs, O, SemExprs, SemO, Var) :-
    Expr =.. [Op, Arg1, Arg2],
    debug(mzn_model, '- extension of binary op ~w', [Expr]),
    constraint_extension(Arg1, Types, Arg1Expr, A1, Arg1Sexprs, SA1, Var1),
    constraint_extension(Arg2, Types, Arg2Expr, A2, Arg2Sexprs, SA2, Var2),
    SemExpr =.. [Op, SA1, SA2],
    append([Arg1Sexprs, Arg2Sexprs, [SemO is SemExpr]], SemExprs),
    append(Var1, Var2, Var),
    (
            Var = [] -> (
                TrExpr =.. [Op, A1, A2],
                append([Arg1Expr, Arg2Expr, [O is TrExpr]], ConstrExprs)
            );(
                (Var1 = [] -> Var1Expr = []; Var1Expr = Arg1Expr),
                (Var2 = [] -> Var2Expr = []; Var2Expr = Arg2Expr),
                append(Var1Expr, Var2Expr, ConstrExprs),
                O = A1
            )
    ).

constraint_args([], [], _, _, _, _) :- debug(mzn_model, '- constraint with no variables involved', []), fail.
constraint_args(L, R, _, _, _, _) :- length(L, LL), length(R, LR), Length is LL + LR, Length > 2, 
                                     debug(mzn_model, '- constraint with to many (~w) variables involved', [Length]), fail.
constraint_args([V - var(Var) - _], [], _, RightOutput, V:var(Var), RightOutput:domain(Var)).
constraint_args([], [V - var(Var) - _], LeftOutput, _, LeftOutput:domain(Var), V:var(Var)).
constraint_args([LV - LType - _], [RV - RType - _], _, _, LV:LType, RV:RType).
constraint_args([LV - LType - _, RV - RType - _], [], _, _, LV:LType, RV:RType).
constraint_args([], [LV - LType - _, RV - RType - _], _, _, LV:LType, RV:RType).

constraint_sem_inputs([_-_-LeftInput], [], _, RightExprList, [], LeftInput, [RightInput is RightExpr], RightInput) :-
    list_to_conjuct(RightExprList, RightExpr).
constraint_sem_inputs([], [_-_-RightInput], LeftExprList, _, [LeftInput is LeftExpr], LeftInput, [], RightInput) :-
    list_to_conjuct(LeftExprList, LeftExpr).
constraint_sem_inputs([_-_-LeftInput], [_-_-RightInput], _, _, [], LeftInput, [], RightInput).
constraint_sem_inputs([], [], LeftExprList, RightExprList, [LeftInput is LeftExpr], LeftInput, [RightInput is RightExpr], RightInput) :-
    list_to_conjuct(LeftExprList, LeftExpr),
    list_to_conjuct(RightExprList, RightExpr).

type_name(var(Name), AtomName) :- term_to_atom(AtomName, Name).
type_name(domain, const).

constraint_name(Op, LType, RType, Name) :-
    type_name(LType, LName), type_name(RType, RName),
    atomic_list_concat([Op, LName, RName, ''], '_', Prefix),
    gensym(Prefix, Name).

constraint_extensional_definition(Name, LeftType, RightType, Args, ExtensionalDefinition) :- 
    ExtensionalDefinition =.. [define_constraint, Name, LeftType-RightType, Args].
constraint_semantical_definition(Name, Constraint, LeftInputExpr, RightInputExpr, LeftInput, RightInput, LeftExpr, RightExpr, LeftOutput, RightOutput, SemanticalDefinition) :-
    constraint_basic(Constraint, Operator),
    CExpr =.. [Operator, LeftOutput, RightOutput],
    (LeftInputExpr = [] -> LeftSide = LeftExpr; LeftSide = []),
    (RightInputExpr = [] -> RightSide = RightExpr; RightSide = []),
    append([LeftSide, RightSide, [CExpr]], CBodyList),
    list_to_conjuct(CBodyList, CBody),
    CHead =.. [Name, LeftInput, RightInput],
    CSemantics = (CHead :- CBody),
    DirectDefinition =.. [define_constraint_semantics, CSemantics],
    append([LeftInputExpr, RightInputExpr, [DirectDefinition]], SemanticalDefinition).

constraint_definition(C, Types, Extension, Semantics) :-
    C =.. [constraint, Op, X, Y],
    constraint_basic(Op, _),
    debug(mzn_model, '- defining ~w with types ~w', [C, Types]),
    constraint_extension(X, Types, LeftExpr, LeftOutput, LeftSemExpr, LeftSemOutput, Var1),
    debug(mzn_model, '- constraint extension ~w -> ~w, ~w, ~w', [X, LeftExpr, LeftOutput, Var1]),
    constraint_extension(Y, Types, RightExpr, RightOutput, RightSemExpr, RightSemOutput, Var2),
    debug(mzn_model, '- constraint extension ~w -> ~w, ~w, ~w', [Y, RightExpr, RightOutput, Var2]),
    debug(mzn_model, '- constraint semantics ~w -> ~w, ~w, ~w', [X, LeftSemExpr, LeftSemOutput, Var1]),
    debug(mzn_model, '- constraint semantics ~w -> ~w, ~w, ~w', [Y, RightSemExpr, RightSemOutput, Var2]),
    constraint_args(Var1, Var2, LeftOutput, RightOutput, LeftArg:LeftType, RightArg:RightType),
    debug(mzn_model, '- constraints args ~w, ~w', [LeftArg:LeftType, RightArg:RightType]),
    constraint_name(Op, LeftType, RightType, Name),
    debug(mzn_model, '- constraint name ~w', [Name]),
    constraint_extensional_definition(Name, LeftType, RightType, [LeftArg-RightArg], ExtensionalDefinition),
    constraint_sem_inputs(Var1, Var2, LeftSemExpr, RightSemExpr, LeftInputExpr, LeftInput, RightInputExpr, RightInput),
    constraint_semantical_definition(Name, Op, LeftInputExpr, RightInputExpr, LeftInput, RightInput, LeftSemExpr, RightSemExpr, LeftSemOutput, RightSemOutput, SemanticalDefinitionList),
    list_to_conjuct(SemanticalDefinitionList, Semantics),
    debug(mzn_model, '- semantical definition ~w', [Semantics]),
    append([LeftExpr, RightExpr, [ExtensionalDefinition]], ExtensionList),
    list_to_conjuct(ExtensionList, Extension).
 
constraint_definition(C, Types, [Loop], [Semantics]) :-
    debug(mzn_model, '- constraint definition of ~w', [C]),
    C =.. [constraint, ident("forall"), GeneratorsList, Body],
    debug(mzn_model, '- universal quantifier detected', []),
    constraint_generators_flatten(GeneratorsList, Generators),
    debug(mzn_model, '- parsing generators ~w', [Generators]),
    constraint_generators(Generators, GeneratorClauses, VariableNames, _),
    debug(mzn_model, '- generators ~w', [GeneratorClauses]),
    constraint_replace_varnames_list(GeneratorClauses, VariableNames, GeneratorClausesWithVars),
    debug(mzn_model, '- generators with vars ~w', [GeneratorClausesWithVars]),
    constraint_replace_varnames(Body, VariableNames, BodyWithVars),
    debug(mzn_model, '- translated body ~w', [BodyWithVars]),
    constraint_definition(BodyWithVars, Types, DirectDefinition, Semantics),
    debug(mzn_model, '- definitions ~w and ~w', [DirectDefinition, Semantics]),
    list_to_conjuct(GeneratorClausesWithVars, GeneratorsConjuct),
    Loop =.. [forall, GeneratorsConjuct, DirectDefinition],
    debug(mzn_model, '- assembled forall constraint ~w \n with semantics ~w', [Loop, Semantics]).

constraint_definition(C, _, [Extension], [Semantics]) :-   
    C =.. [call, CNameQuoted, CArgsQuoted],
    term_to_atom(CName, CNameQuoted),
    constraint_global(CName, CArgsQuoted, Extension, Semantics).

constraint_replace_varnames_list([], _, []) :- !.
constraint_replace_varnames_list([H|T], VariableNames, [TH|TT]) :- !,
    constraint_replace_varnames(H, VariableNames, TH),
    constraint_replace_varnames_list(T, VariableNames, TT).
constraint_replace_varnames(Term, _, Term) :- 
    var(Term), !.
constraint_replace_varnames(Term, VariableNames, TTerm) :-
    get_assoc(Term, VariableNames, TTerm), !.
constraint_replace_varnames(Term, VariableNames, TTerm) :-
    Term =.. [Name | Args], !,
    constraint_replace_varnames_list(Args, VariableNames, TArgs),
    TTerm =.. [Name | TArgs].
constraint_replace_varnames(Term, _, Term).

constraint_generator(in(VarName, Set), in(Var,Set), VarName-Var:range(Set)).

constraint_array_get_translate(Var, array_get(Name, array(Index)), ArrayGet) :-
    append([[Name], Index, [Var]], ArrayGetList),
    ArrayGet =.. ArrayGetList.
constraint_guard_translate(Exp, Exp) :- Exp =.. [Op | _], Op \= array_get.

constraint_guard(is(Var, Exp), is(Var, PrologExpr)) :-
    constraint_guard_translate(Exp, PrologExpr).
constraint_guard(is(Var, Exp), PrologExpr) :-
    constraint_array_get_translate(Var, Exp, PrologExpr).
constraint_guard(Guard, ReifiedGuard) :-
    Guard =.. [guard, Test | Args],
    constraint_basic(Test, ReifiedTest),
    ReifiedGuard =.. [ReifiedTest | Args].

constraint_generators_flatten([generator(G)], G).
constraint_generators_flatten([generator(G)|R], T) :-
    constraint_generators_flatten(R, TR),
    append(G, TR, T).

constraint_generators([], [], VarMap, TypeMap) :- empty_assoc(VarMap), empty_assoc(TypeMap).
constraint_generators([G|Gs], [T|Ts], VarMap, TypeMap) :-
    debug(mzn_model, '- trying generators ~w', [G]),
    constraint_generator(G, T, Name-Var:Type),
    debug(mzn_model, '- success ~w', [T]),
    constraint_generators(Gs, Ts, OldVarMap, OldTypeMap),
    put_assoc(Name, OldVarMap, Var, VarMap),
    put_assoc(Name, OldTypeMap, Type, TypeMap).
constraint_generators([G|Gs], [T|Ts], VarMap, TypeMap) :-
    debug(mzn_model, '- trying guard ~w', [G]),
    constraint_guard(G, T),
    debug(mzn_model, '- success ~w', [T]),
    constraint_generators(Gs, Ts, VarMap, TypeMap).


constraint_basic(gte, >=).
constraint_basic(gt, >).
constraint_basic(lte, =<).
constraint_basic(lt, <).
constraint_basic(neq, \==).
constraint_basic(eq, ==).

constraint_global(alldifferent, [QuotedType], Extension, Semantics) :-
    term_to_atom(Type, QuotedType),
    atomic_list_concat([alldifferent, Type, ''], '_', Prefix),
    gensym(Prefix, Name),
    Extension = (get_index_set(Type, 1, IndexMin..IndexMax), forall((between(IndexMin, IndexMax, X), between(IndexMin, IndexMax, Y), X \= Y, VX =.. [Type, X], VY =.. [Type, Y]), define_constraint(Name, var(Type)-var(Type), [VX-VY]))),
    SemHead =.. [Name, Arg1, Arg2],
    Semantics = define_constraint_semantics((SemHead :- get_value(Arg1, Val1), get_value(Arg2, Val2), Val1 \== Val2)).
constraint_global(circuit, [QuotedType], Extension, Semantics) :-
    term_to_atom(Type, QuotedType),
    atomic_list_concat([circuit, Type], '_', Prefix),

    atomic_list_concat([Prefix, var, ''], '_', VarNamePrefix),
    gensym(VarNamePrefix, VarName),
    VarDeclaration = [define_variable(VarName, [domain(Type)], domain(Type))],

    atomic_list_concat([Prefix, selfdiff, ''], '_', SelfDiffNamePrefix),
    gensym(SelfDiffNamePrefix, SelfDiffName),
    SelfDiffExtension = [findall(N-I, variable(Type, I, N), NextIndexPairs), define_constraint(SelfDiffName, var(Type) - domain(Type), NextIndexPairs)],
    SelfDiffSemanticsHead =.. [SelfDiffName, SA1, SA2],
    SelfDiffSemantics = define_constraint_semantics((SelfDiffSemanticsHead :- get_value(SA1, SA1VAL), SA1VAL \== SA2)),

    constraint_global(alldifferent, [QuotedType], AllDiffExtension, AllDiffSemantics),
    constraint_global(alldifferent, [VarName], AllNextDiffExtension, AllNextDiffSemantics),

    DependencyExtension = [findall(DO-DN, (variable(VarName, _, DO), variable(Type, _, DN)), DOtoDN), make_variable_depend_on(VarName, var(Type), DOtoDN)],
    DependencySemanticsHead =.. [VarName, DA1, DA2, DR],
    DependencySemanticsMinIndex = get_index_set(Type, 1, DA1MinIndex.._),
    DependencySemantics = define_dependency_semantics((DependencySemanticsHead :- 
        variable(VarName, DA1Index, DA1),
        
        if(DA1Index = DA1MinIndex, (
            DR is DA1MinIndex
        ), (
            PrevIndex is DA1Index - 1,
            variable(VarName, PrevIndex, PrevDA1),
            get_value(PrevDA1, PrevDA1Value),
            variable(Type, PrevDA1Value, DA2),
            get_value(DA2, DA2Value),
            DR is DA2Value   
        )))),
    
    append([VarDeclaration, SelfDiffExtension, [AllDiffExtension], [AllNextDiffExtension], DependencyExtension], ExtensionList),
    list_to_conjuct(ExtensionList, Extension),
    SemanticsList = [SelfDiffSemantics, AllDiffSemantics, AllNextDiffSemantics, DependencySemanticsMinIndex, DependencySemantics],
    list_to_conjuct(SemanticsList, Semantics).

% #endregion constraints

% define_variable(order, [range(cities)], range(cities)),



