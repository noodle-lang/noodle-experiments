#!/usr/bin/env swipl

:- use_module(library(noodle)).
:- use_module(library(assoc)).
:- use_module(library(yall)).

:- initialization(main,main).
:- dynamic grammar_element/1.


main([DomainPath]) :-
    consult(DomainPath),
    grammar_new(Grammar),
    grammar_print(Grammar).

grammar_new(Grammar) :-
    grammar_empty(G),
    grammar_add_neighbor_clause(G, G1),
    grammar_add_move_clause(G1, G2),
    grammar_add_getter_clause(G2, G3),
    grammar_add_model(G3, Grammar).

grammar_empty(Grammar) :-
    empty_assoc(Grammar).

grammar_add_neighbor_clause(G, NG) :-
    grammar_add_clause(G, '<program>', '(neighbor :- <move-get-top>, <move-top>).', G1),
    grammar_add_clause(G1, '<program-sub>', '(<move>)', NG).

grammar_add_move_clause(G, NG) :-
    grammar_add_clauses(G, '<move-top>', ['<move-atomic-top>, <move-top>', '<move-meta>', '<move-set-top>'], G1),
    grammar_add_clauses(G1, '<move>', ['<move-atomic>, <move>', '<move-set>'], G2),
    grammar_add_clauses(G2, '<move-atomic-top>', ['<move-set-top>', '<move-filter-top>', '<move-meta>'], G3),
    grammar_add_clauses(G3, '<move-atomic>', ['<move-set>', '<move-filter>', '<move-get>'], NG).
    

grammar_add_getter_clause(G, NG) :-
    grammar_add_clauses(G, '<move-get-top>', ['<move-atomic-get-top>, <move-get-top>', '<move-atomic-get-top>'], NG).

grammar_add_model(G, NG) :-
    findall(Constraint, available_constraint(Constraint), Constraints),
    grammar_add_constraints(G, Constraints, G1), 
    grammar_add_index_ranges(G1, NG).

grammar_add_index_ranges(G, NG) :-
    grammar_add_clause(G, '<i-variables>', 'GE_RANGE:4', G1),
    grammar_max_range_occurences(TotalOccurences, SingleCallOccurences),
    RangeOccurences  is 4 * TotalOccurences,
    format(atom(I), 'GE_RANGE:~w', [RangeOccurences]),
    grammar_add_clause(G1, '<i-values>', I, G2),
    MetaRangeOccurences is 2 * SingleCallOccurences,
    format(atom(MI), 'GE_RANGE:~w', [MetaRangeOccurences]),
    grammar_add_clause(G2, '<i-values-meta>', MI, G3),
    grammar_add_clause(G3, '<i-variables-meta>', 'GE_RANGE:2', NG).

grammar_max_range_occurences(TotalOccurences, SingleCallOccurences) :-
    aggregate_all(max(X), (type_uses(range(_), Uses), length(Uses, X)), TotalOccurences),
    findall(R, (
        range_definition(Range, _),  
        bagof(Range, Index^type_of(index(_, Index), range(Range)), Occurrences), 
        length(Occurrences,R)
        ), 
    RangeOccurrences), 
    aggregate_all(max(X), member(X, RangeOccurrences), SingleCallOccurences).



grammar_add_constraints(G, [], G).
grammar_add_constraints(G, [Constraint | Rest], NG) :-
    once(grammar_add_constraint(G, Constraint, TG)),
    grammar_add_constraints(TG, Rest, NG).

grammar_add_constraint(G, Constraint, NG) :-
    type_of(arg(Constraint, 1), LeftArg),
    type_of(arg(Constraint, 2), RightArg),
    grammar_add_element(G, LeftArg, G1),
    grammar_add_element(G1, RightArg, G2),
    grammar_add_constraint_checks(G2, Constraint, G3),
    grammar_add_constraint_getter(G3, Constraint, G4),
    grammar_add_constraint_walk_over(G4, Constraint, G5),
    grammar_add_constraint_for_each(G5, Constraint, NG).
grammar_add_constraint(G, _, G).


grammar_element_name(Element, Token) :-
    Element =.. [Type, Name],
    format(atom(UpperToken), '<~w-~w>', [Type, Name]),
    string_lower(UpperToken, Token).
grammar_element_top_name(Element, Token) :- 
    Element =.. [Type, Name],
    format(atom(UpperToken), '<~w-~w-top>', [Type, Name]),
    string_lower(UpperToken, Token).
grammar_element_meta_name(Element, Token) :-
    Element =.. [Type, Name],
    format(atom(UpperToken), '<~w-~w-meta>', [Type, Name]),
    string_lower(UpperToken, Token).

grammar_element_meta_var_index(range(_), 'i-values-meta').
grammar_element_meta_var_index(var(_), 'i-variables-meta').
grammar_element_var_index(range(_), 'i-values').
grammar_element_var_index(var(_), 'i-variables').

grammar_element_var_generator(Element, Token) :-
    Element =.. [Type, Name],
    format(atom(LowerToken), '~w_~w', [Type, Name]),
    string_upper(LowerToken, UpperToken),
    grammar_element_var_index(Element, Index),
    format(atom(Token), '~w_<~w>', [UpperToken, Index]).
grammar_element_var(Element, Index, Token) :-
    Element =.. [Type, Name],
    format(atom(LowerToken), '~w_~w_~w', [Type, Name, Index]),
    string_upper(LowerToken, Token).
grammar_element_meta_var_generator(Element, Token) :-
    Element =.. [Type, Name],
    format(atom(LowerToken), '~w_~w_meta', [Type, Name]),
    string_upper(LowerToken, UpperToken),
    grammar_element_meta_var_index(Element, Index),
    format(atom(Token), '~w_<~w>', [UpperToken, Index]).
grammar_element_meta_var(Element, Index, Token) :-
    Element =.. [Type, Name],
    format(atom(LowerToken), '~w_~w_meta_~w', [Type, Name, Index]),
    string_upper(LowerToken, Token).
grammar_var_index_types(Name, IndexTypes) :-
    available_variable(Name/Arity),
    findall(I, between(1, Arity, I), Indexes),
    maplist([I,O] >> type_of(index(Name, I), O), Indexes, IndexTypes).
grammar_get_name(Element, NamePredicate, Name) :-
    NameCall =.. [NamePredicate, Element, Name],
    call(NameCall).

grammar_generate_index(X) :- between(0, 24, X).
grammar_generate_meta_vars(IndexTypes, MetaVars) :-
    grammar_generate_meta_vars(IndexTypes, [], MetaVars).
grammar_generate_meta_vars([], _, []).
grammar_generate_meta_vars([IT|IndexTypes], Visited, [MV|MetaVars]) :-
    grammar_generate_index(Index),
    grammar_element_meta_var(IT, Index, MV),
    \+ member(MV, Visited),
    !,
    grammar_generate_meta_vars(IndexTypes, [MV|Visited], MetaVars).

grammar_add_element_meta_vars(G, Element, NG) :-
    grammar_element_meta_name(Element, MetaName),
    grammar_element_meta_var_generator(Element, MetaVarGenerator),
    grammar_add_clause(G, MetaName, MetaVarGenerator, NG).
grammar_add_element_top_vars(G, Element, NG) :-
    grammar_element_top_name(Element, VarName),
    grammar_element_var_generator(Element, VarGenerator),
    grammar_add_clause(G, VarName, VarGenerator, NG).
grammar_add_element_vars(G, Element, NG) :-
    grammar_element_name(Element, AllName),
    grammar_element_meta_name(Element, MetaName),
    grammar_element_top_name(Element, VarName),
    grammar_add_clauses(G, AllName, [MetaName, VarName], NG).

grammar_add_getters(G, Element, NG) :-
    grammar_add_range_getters(G, Element, '<move-atomic-get-top>', grammar_element_top_name, NG).
    % # grammar_add_range_getters(G1, Element, '<move-get>', grammar_element_name, NG).
grammar_add_getters(G, Element, NG) :-
    grammar_add_var_getters(G, Element, '<move-atomic-get-top>', grammar_element_top_name, NG).
    % # grammar_add_var_getters(G1, Element, '<move-get>', grammar_element_name, NG).

grammar_add_range_getters(G, Element, Clause, NamePredicate, NG) :-
    Element = range(Name),
    grammar_get_name(Element, NamePredicate, Var),
    format(atom(Get), 'range_element("\'~w\'", ~w)', [Name, Var]),
    grammar_add_clause(G, Clause, Get, NG).

grammar_add_var_getters(G, Element, Clause, NamePredicate, NG) :-
    Element = var(Name),
    grammar_get_name(Element, NamePredicate, Var),
    grammar_var_index_types(Name, IndexTypes),
    maplist(NamePredicate, IndexTypes, IndexTokens),
    atomic_list_concat(IndexTokens, ", ", Args),
    format(atom(Get), 'variable("\'~w\'", ~w, ~w)', [Name, Args, Var]),
    grammar_add_clause(G, Clause, Get, NG).

grammar_add_value_getters(G, Element, NG) :-
    grammar_add_value_generators(G, Element, '<move-atomic-get-top>', grammar_element_top_name, G1),
    grammar_add_value_getters(G1, Element, '<move-get>', grammar_element_name, NG).

grammar_add_value_generators(G, Element, Clause, NamePredicate, NG) :-
    grammar_add_value_inputs(G, Element, Clause, NamePredicate, 'value', NG).

grammar_add_value_getters(G, Element, Clause, NamePredicate, NG) :-
    grammar_add_value_inputs(G, Element, Clause, NamePredicate, 'get_value', NG).

grammar_add_value_inputs(G, Element, Clause, NamePredicate, Type, NG) :-
    Element = var(Name),
    grammar_get_name(Element, NamePredicate, Var),
    type_of(domain(Name), ValueType),
    grammar_get_name(ValueType, NamePredicate, Val),
    format(atom(Get), '~w("\'~w\'", ~w, ~w)', [Type, Name, Var, Val]),
    grammar_add_clause(G, Clause, Get, NG).

grammar_add_setters(G, Element, G) :-
    Element = var(Name),
    (fixed(Name); auxillary(Name, _); reified(Name)), !.
grammar_add_setters(G, Element, NG) :-
    grammar_add_setters(G, Element, '<move-set-top>', grammar_element_top_name, G1),
    grammar_add_setters(G1, Element, '<move-set>', grammar_element_name, NG).
grammar_add_setters(G, Element, Clause, NamePredicate, NG) :-
    Element = var(Name),
    grammar_get_name(Element, NamePredicate, Var),
    type_of(domain(Name), ValueType),
    grammar_get_name(ValueType, NamePredicate, Val),
    format(atom(Flip), 'flip_variable(~w, ~w, ~w)', [Var, Val, Val]),
    format(atom(Swap), 'swap_values(~w, ~w)', [Var, Var]),
    format(atom(Set), 'set_value(~w, ~w)', [Var, Val]),
    grammar_add_clauses(G, Clause, [Flip, Swap, Set], NG).

grammar_add_eq_operators(G, Element, NG) :-
    grammar_add_eq_operators(G, Element, '<move-filter>', grammar_element_name, G1),
    grammar_add_eq_operators(G1, Element, '<move-filter-top>', grammar_element_top_name, NG).
grammar_add_eq_operators(G, Element, Clause, NamePredicate, NG) :-
    grammar_get_name(Element, NamePredicate, Name),
    format(atom(EqClause), '~w = ~w', [Name, Name]),
    format(atom(NeqClause), '~w "\\=" ~w', [Name, Name]),
    grammar_add_clauses(G, Clause, [EqClause, NeqClause], NG).

grammar_add_comparator(G, Element, NG) :-
    grammar_add_comparator(G, Element, '<move-filter>', grammar_element_name, G1),
    grammar_add_comparator(G1, Element, '<move-filter-top>', grammar_element_top_name, NG).
grammar_add_comparator(G, Element, Clause, NamePredicate, NG) :-
    grammar_get_name(Element, NamePredicate, Name),
    format(atom(LessClause), '~w "<" ~w', [Name, Name]),
    grammar_add_clause(G, Clause, LessClause, NG).

grammar_add_for_each(G, Element, NG) :-
    Element = range(Name),
    grammar_element_meta_var(Element, 0, MetaVar0),
    format(atom(ForEach), 'for_each(range_element("\'~w\'", ~w), <program-sub>)', [Name, MetaVar0]),
    grammar_add_clause(G, '<move-meta>', ForEach, NG).
grammar_add_for_each(G, Element, NG) :-
    Element = var(Name),
    grammar_var_index_types(Name, IndexTypes),
    grammar_generate_meta_vars(IndexTypes, MetaVars),
    atomic_list_concat(MetaVars, ", ", Args),
    grammar_element_meta_var(Element, 0, MetaVar0),
    format(atom(ForEach), 'for_each(variable("\'~w\'", ~w, ~w), <program-sub>)', [Name, Args, MetaVar0]),
    grammar_add_clause(G, '<move-meta>', ForEach, NG).

grammar_add_iterate(G, Element, NG) :-
    Element = var(Name),
    available_variable(Name/1),
    type_of(index(Name, 1), Type),
    type_of(domain(Name), Type),
    !,
    grammar_element_top_name(Element, GrammarName),
    grammar_element_meta_var(Type, 0, MetaIndex0),
    grammar_element_meta_var(Type, 1, MetaIndex1),
    grammar_element_meta_var(Element, 0, MetaVar0),
    grammar_element_meta_var(Element, 1, MetaVar1),
    format(atom(Iterate), 'iterate(variable("\'~w\'", ~w, ~w) - variable("\'~w\'", ~w, ~w), ~w, <program-sub>)', 
        [Name, MetaIndex0, MetaVar0, Name, MetaIndex1, MetaVar1, GrammarName]),
    format(atom(IterateBackward), 'iterate_backward(variable("\'~w\'", ~w, ~w) - variable("\'~w\'", ~w, ~w), ~w, <program-sub>)', 
        [Name, MetaIndex0, MetaVar0, Name, MetaIndex1, MetaVar1, GrammarName]),
    grammar_add_clauses(G, '<move-meta>', [Iterate, IterateBackward], NG).    
grammar_add_iterate(G, _, G).


grammar_add_element(G, El, G) :-
    grammar_element(El),
    !.
grammar_add_element(G, Element, NG) :-
    Element = range(_),
    assert(grammar_element(Element)), 
    grammar_add_element_meta_vars(G, Element, G1),
    grammar_add_element_vars(G1, Element, G2),
    grammar_add_element_top_vars(G2, Element, G3),
    grammar_add_getters(G3, Element, G4),
    grammar_add_eq_operators(G4, Element, G5),
    grammar_add_comparator(G5, Element, G6),
    grammar_add_for_each(G6, Element, NG).

grammar_add_element(G, Element, NG) :-
    Element = var(VarName),
    \+ auxillary(VarName, _),
    assert(grammar_element(Element)),
    grammar_add_element_meta_vars(G, Element, G1),
    grammar_add_element_vars(G1, Element, G2),
    grammar_add_element_top_vars(G2, Element, G3),
    grammar_add_eq_operators(G3, Element, G4),
    grammar_add_getters(G4, Element, G5),
    grammar_add_value_getters(G5, Element, G6),
    grammar_add_for_each(G6, Element, G7),
    grammar_add_setters(G7, Element, G8),
    grammar_add_iterate(G8, Element, G9),
    grammar_add_var_domain(G9, VarName, G10),
    grammar_add_var_indices(G10, VarName, NG).

grammar_add_var_domain(G, Variable, NG) :-
    type_of(domain(Variable), Domain),
    grammar_add_element(G, Domain, NG).

grammar_add_var_indices(G, Variable, NG) :-
    findall(Index, type_of(index(Variable, _), Index), Indices),
    foldl(([I,GAcc,NGAcc] >> grammar_add_element(GAcc, I, NGAcc)), Indices, G, NG).
    
grammar_add_constraint_checks(G, Constraint, NG) :-
    grammar_add_constraint_checks(G, Constraint, '<move-filter-top>', grammar_element_top_name, G1),
    grammar_add_constraint_checks(G1, Constraint, '<move-filter>', grammar_element_name, G2),
    grammar_add_constraint_basic_clause(G2, Constraint, '<move-filter>', grammar_element_name, are_constrained, NG).

grammar_add_constraint_checks(G, Constraint, Clause, NamePredicate, NG) :-
    grammar_add_constraint_basic_clause(G, Constraint, Clause, NamePredicate, is_satisfied, G1),
    grammar_add_constraint_basic_clause(G1, Constraint, Clause, NamePredicate, is_violated, NG).

grammar_add_constraint_getter(G, Constraint, NG) :-
    grammar_add_constraint_getter(G, Constraint, '<move-atomic-get-top>', grammar_element_top_name, NG).
grammar_add_constraint_getter(G, Constraint, Clause, NamePredicate, NG) :-
    grammar_add_constraint_basic_clause(G, Constraint, Clause, NamePredicate, constraint, NG).

grammar_add_constraint_basic_clause(G, Constraint, Clause, NamePredicate, Predicate, NG) :-
    type_of(arg(Constraint, 1), LeftArg),
    type_of(arg(Constraint, 2), RightArg),
    grammar_get_name(LeftArg, NamePredicate, LeftName),
    grammar_get_name(RightArg, NamePredicate, RightName),
    format(atom(Body), '~w("\'~w\'", ~w, ~w)', [Predicate, Constraint, LeftName, RightName]),
    grammar_add_clause(G, Clause, Body, NG).

grammar_add_constraint_walk_over(G, Constraint, NG) :-
    type_of(arg(Constraint, 1), LeftArg),
    type_of(arg(Constraint, 2), RightArg),
    grammar_element_meta_var(LeftArg, 0, MetaVar0),
    grammar_element_meta_var(RightArg, 1, MetaVar1),
    grammar_element_top_name(LeftArg, LeftStart),
    grammar_element_top_name(RightArg, RightStart),
    format(atom(Walk), 'walk_over(constraint("\'~w\'", ~w, ~w), ~w, <program-sub>)', [Constraint, MetaVar0, MetaVar1, LeftStart]),
    format(atom(WalkInversed), 'walk_over_inversed(constraint("\'~w\'", ~w, ~w), ~w, <program-sub>)', [Constraint, MetaVar0, MetaVar1, RightStart]),
    grammar_add_clauses(G, '<move-meta>', [Walk, WalkInversed], NG).

grammar_add_constraint_for_each(G, Constraint, NG) :-
    type_of(arg(Constraint, 1), LeftArg),
    type_of(arg(Constraint, 2), RightArg),
    grammar_element_top_name(LeftArg, LeftName),
    grammar_element_top_name(RightArg, RightName),
    format(atom(Body), 'for_each(constraint("\'~w\'", ~w, ~w), <program-sub>)', [Constraint, LeftName, RightName]),
    grammar_add_clause(G, '<move-meta>', Body, NG).

grammar_add_clauses(G, Head, Clauses, NG) :-
    get_assoc(Head, G, OldClauses),
    !,
    append(OldClauses, Clauses, NewClauses),
    put_assoc(Head, G, NewClauses, NG).
grammar_add_clauses(G, Head, Clauses, NG) :-
    put_assoc(Head, G, Clauses, NG).

grammar_add_clause(G, Head, Clause, NG) :-
    grammar_add_clauses(G, Head, [Clause], NG).

grammar_print(Grammar) :-
    del_assoc('<program>', Grammar, MainClause, G),
    grammar_print_rule('<program>', MainClause),
    assoc_to_keys(G, NonTerminals),
    sort(NonTerminals, SortedNonTerminals),
    forall(member(NT, SortedNonTerminals), (
        get_assoc(NT, G, Rules),
        grammar_print_rule(NT, Rules)
    )).

grammar_print_rule(Head, Rules) :-
    sort(Rules, SortedRules),
    atomic_list_concat(SortedRules, '\n\t| ', Body),
    format('~w ::= ~t~w~n~n', [Head, Body]).

    

